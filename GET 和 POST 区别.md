# GET 和 POST 区别

[TOC]

## 浏览器的GET POST

**GET**: 

幂等，可被缓存

浏览器直接发出的GET只能由一个url触发：query携带参数

**POST**: 

非幂等，有副作用

POST请求都来自表单提交，表单的数据被浏览器用编码到HTTP请求的body

## 接口中的GET POST

接口API：从协议本身看，并没有什么限制说GET一定不能没有body，POST就一定不能把参放到<URL>的querystring上，但这样会造成混乱——需约定接口 ：**REST API**

REST充分运用GET、POST、PUT和DELETE，约定了这4个接口分别**获取、创建、替换和删除**“资源”，REST最佳实践还推荐在请求体使用**json**格式

json相对于x-www-form-urlencoded的优势在于1）可以有嵌套结构；以及 2）可以支持更丰富的数据类型。通过一些框架，json可以直接被服务器代码映射为业务实体。用起来十分方便

## GET POST 安全性

POST用body传输数据，而GET用url传输，更加容易（被客户端到服务器端的中间节点，包括网关，代理）看到

但对于攻击者，两者皆为明文

**必须做从客户端到服务器的端端加密。业界的通行做法就是https**——即用SSL协议协商出的密钥加密明文的http数据

## 编码

GET 和 POST 原则上都可以有url和body

### url 编码：

不经编码显示：ASCII的子集`[a-zA-Z0-9$-_.+!*'(),]`

编码 encodeURI ：**%hex** (percent encode)

### Body 编码

HTTP Body相对好些，因为有个`Content-Type`来比较明确的定义。比如：

```text
POST xxxxxx HTTP/1.1
...
Content-Type: application/x-www-form-urlencoded ; charset=UTF-8
```

这里Content-Type会同时定义请求body的格式（application/x-www-form-urlencoded）和字符编码（UTF-8）。

所以body和url都可以提交中文数据给后端，但是POST的规范好一些，相对不容易出错，容易让开发者安心。对于GET+url的情况，只要不涉及到在老旧浏览器的地址栏输入url，也不会有什么太大的问题。

浏览器直接发出的POST请求就是表单提交: application/x-www-form-urlencoded针对简单的key-value场景；和multipart/form-data，针对只有文件提交，或者同时有文件和key-value的混合提交表单的场景。

如果是Ajax或者其他HTTP Client发出去的POST请求，其body格式就非常自由了，常用的有json，xml，文本，csv……甚至是你自己发明的格式。只要前后端能约定好即可。

## URL长度限制

“GET数据有长度限制“其实是指”URL的长度限制“。

HTTP协议本身对URL长度并没有做任何规定。实际的限制是由客户端/浏览器以及服务器端决定的。

## References

https://www.zhihu.com/question/28586791/answer/767316172