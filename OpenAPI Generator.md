

# OpenApi Generator



Frontend engineers usually have to deal with a large number of APIs that are changing throughout the developing process. It is tedious and inefficient having to make API calls manually such as `axios.request('get', url, params)` every where in your code. Apart from that, manually changing the API layer whenever there are new changes made to the APIs is also inefficient and often lead to inconsistencies and errors. 

Nowadays many teams are using API managing tools such as Postman, Swagger and YAPI. These tools follows the OpenAPI Specification (OAS) and can export description files in  JSON/YAML. It woud be much more efficient if frontend and backend engineers can communicate about API changes using these standardized tools.

Therefore we'd like to find a way that would make API managing and calling much easier:

- To follow the changes of APIs using OpenAPI Specification in a consistent way
- To generate and manage API calling function codes automatically according to OAS.



[TOC]



### What Is OpenAPI?

**OpenAPI Specification** (**OAS**, formerly Swagger Specification) is an API description format for REST APIs. An OpenAPI file allows you to describe your entire API, including:

- Available endpoints (`/users`) and operations on each endpoint (`GET /users`, `POST /users`)
- Operation parameters Input and output for each operation
- Authentication methods
- Contact information, license, terms of use and other information.

API specifications can be written in YAML or JSON. The format is easy to learn and readable to both humans and machines. The complete OpenAPI Specification can be found on GitHub: [OpenAPI 3.0 Specification](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)



Here is an example of OpenAPI Specification file, and how it corresponds to API docs:  [Swagger Editor](https://editor.swagger.io/)

### What Is Swagger?

**Swagger** is a set of open-source tools built around the OpenAPI Specification that can help you design, build, document and consume REST APIs. The major Swagger tools include:

- [Swagger Editor](http://editor.swagger.io/) – browser-based editor where you can write OpenAPI specs.
- [Swagger UI](https://swagger.io/swagger-ui/) – renders OpenAPI specs as interactive API documentation.
- [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) – generates server stubs and client libraries from an OpenAPI spec.



### What is OpenAPI Generator?

[OpenAPI Generator](https://openapi-generator.tech/) can consume **openAPI specs** (JSON/YAML) exported from API platforms such as YAPI, and generate functions of given language that can be used for calling APIs. 



To install the tool as a dev dependency in your current project:

`npm install @openapitools/openapi-generator-cli -D`



For more info on how to install and use it, please refer to the official docs: 

> CLI Installation https://openapi-generator.tech/docs/installation

> Usage https://openapi-generator.tech/docs/usage



### Using openAPI Generator for JS/TS projects

Once we have installed the openAPI Generator, we can start using it to generate our code for API callings. To do that, we need to do two things:

1. Acquire the **OAS spec file** (e.g. export it in JSON/YAML format from an API managing tool such as YAPI or Postman)
2. Write a **command line** that uses the correct generator and output the generated code to a location of your desire.

openAPI has many generators for different target languages ( see [Generators List](https://openapi-generator.tech/docs/generators) ), what we need are the generators for js/ts:

> generator `javascript` uses `superagent`
> generator `typescript-axios` uses `axios` (recommended since `axios` is more familiar)

And we use the following npx command line to generate the api function codes:

`npx @openapitools/openapi-generator-cli generate -i [\*json file path\*] -g [\* client generator\*] -o [\*folder path where you want to generate the code\*]`

- `-g` to specify the generator
- `-o` to specify a meaningful output directory (defaults to the current directory!)
- `-i` to specify the input OpenAPI document



Suppose we have an OAS file called `openapi.json` in the root folder, and we want our api functions generated into `./src/apis/`, here is the command we should use in npm:



   ` npx @openapitools/openapi-generator-cli generate -i openapi.json -g typescript-axios -o ./src/apis`



> For ts generator here is a very detailed tutorial: [Integrate OpenApi specification using openapi-generator to a ReactJs project with Typescript and Axios](https://jiratech.com/media/posts/integrate-openapi-specification-using-openapi-generator-to-a-reactjs-project-with-typescript-and-axios)



### How does OpenAPI Generator generates the codes?

Most of the details of the generation process are irrelevant for code users. All you need to know is how to use a certain api as a function call in the generated code. In OAS file, the apis are described under the field `"paths"` as *path items*. For example, we have an API described as follows:

> **POST** `/pet/{petId}` - Updates a pet in the store with form data
> Parameters:
> `petId` : string
>
> Request body:
>
> ```json
 {
 "name": "string",
 "status": "string"
 }
> ```



The above api will be represented in OAS as follows:

```json
"/pet/{petId}": {
  "post": {
        "tags": [
          "pet"
        ],
        "summary": "Updates a pet in the store with form data",
        "operationId": "updatePetWithForm",
        "parameters": [
          {
            "name": "petId",
            "in": "path",
            "description": "ID of pet that needs to be updated",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/x-www-form-urlencoded": {
              "schema": {
                "properties": {
                  "name": {
                    "type": "string",
                    "description": "Updated name of the pet"
                  },
                  "status": {
                    "type": "string",
                    "description": "Updated status of the pet"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "405": {
            "description": "Invalid input",
            "content": {}
          }
        },
        "security": [
          {
            "petstore_auth": [
              "write:pets",
              "read:pets"
            ]
          }
        ]
      },
},

```

We need to pay attention to these two fields:

`"tags": ["pet"]`: this is the name of the the class of which the api function belongs to. 

`"operationId": "updatePetWithForm"`： identify each operation in the global scope, case-sensitive. `"updatePetWithForm"` follows function naming convention, since this will also be the name of the API calling function.



The above API will eventually yield something like this in `api.ts`:

```ts
class PetApi {
  /**
     * 
     * @summary Updates a pet in the store with form data
     * @param {number} petId ID of pet that needs to be updated
     * @param {string} [name] Updated name of the pet
     * @param {string} [status] Updated status of the pet
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof PetApi
     */
    public updatePetWithForm(petId: number, name?: string, status?: string, options?: any) {
    }
}
```





### How to use the generated codes

After generation is completed, you should see the following files in your project directory:

```
src/apis
├── .gitignore
├── .npmignore
├── .openapi-generator
│   ├── FILES
│   └── VERSION
├── .openapi-generator-ignore
├── api.ts
├── base.ts
├── common.ts
├── configuration.ts
├── git_push.sh
└── index.ts
```



The api function definitions are in `api.ts`, and have already been exported through `index.ts`, all you need to do is to import the class into your code and instantiate it:

```ts
import {PetApi} from "./apis";

const petApi = new PetApi()
```

And you may use the api call function like this:

```ts
petApi.updatePetWithForm(123, 'miao', 'okay');
```



The class constructor accepts 3 arguments:

```ts
const petApi = new PetApi(configuration?, basePath, axiosInstance);
```



* **configuration**: a config obj that allows further request config, e.g.  apiKey, accessToken etc. Optional 
* **basePath**: base url of http request. Will use the default url in your OAS file if empty
* **axiosInstance**: an axios instance to be used for requests. Will use `globalAxios` if empty

If you want to write the config for the api call (ex: headers, timeout, baseUrl), or add interceptors for the calls (request/response interceptors), you can **create an axios instance**: `axiosInstance: AxiosInstance = axios.create({…})`. At this instance you can add interceptors and other config specifications.

After you configured your axiosInstance you can add it as a parameter when use instantiate `petApi`

```ts
AxiosInstance = axios.create({…})
const userApi = new UserApi(null, BASE_URL, axiosInstance);
```
