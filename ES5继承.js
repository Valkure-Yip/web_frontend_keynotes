function Super(foo) {
	this.foo = foo;
}
Super.prototype.printFoo = function () {
	console.log(this.foo);
};
function Sub(bar) {
	this.bar = bar;
	// 1. 调用父类的构造函数
	Super.call(this);
}
// 2. 将子类是原型绑定在父类原型的复制对象上，之后修改子类原型就不会影响父类原型
Sub.prototype = Object.create(Super.prototype);
// 3. 让子类原型的构造函数指向子类构造函数
Sub.prototype.constructor = Sub;