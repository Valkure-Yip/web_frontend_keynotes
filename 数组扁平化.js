
/**
 * es6提供的新方法 flat(depth)
 * @param {*} arr 
 * @returns 
 */
function flatten_1(arr){
	return arr.flat(3)
}

/**
 * 递归
 * @param {*} arr 
 * @returns 
 */
function flatten_2(arr){
	let res = [];
	arr.forEach((val)=>{
		if(val instanceof Array){
			res = res.concat(flatten_2(val))
		} else{
			res.push(val)
		}
	})
	return res
}

/**
 * toString
 * @param {*} arr 
 * @returns 
 */
function flatten_3(arr){
	return arr.toString().split(',').map(val=>{
		return parseInt(val)
	})
}

var arr = [1,[2,3,[4,[5]]]];
console.log(JSON.stringify(flatten_1(arr)))
console.log(JSON.stringify(flatten_2(arr)))
console.log(JSON.stringify(flatten_3(arr)))