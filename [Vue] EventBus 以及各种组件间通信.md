# [Vue] EventBus & 组件间通信

[TOC]

## EventBus

本质上是**发布订阅**

### 使用

**初始化**

首先需要创建事件总线并将其导出，以便其它模块可以使用或者监听它。我们可以通过两种方式来处理。先来看第一种，新创建一个 .js 文件，比如 `event-bus.js`

```js
// event-bus.js
import Vue from 'vue'
export const EventBus = new Vue()
```

实质上`EventBus`是一个不具备 `DOM` 的组件，它具有的仅仅只是它实例方法而已，因此它非常的轻便。

另外一种方式，可以直接在项目中的 `main.js` 初始化 `EventBus` :

```js
// main.js
Vue.prototype.$EventBus = new Vue()
```

注意，这种方式初始化的`EventBus`是一个`全局的事件总线`。

**发送事件**

假设你有两个Vue页面需要通信： A 和 B ，A页面 在按钮上面绑定了点击事件，发送一则消息，想通知 B页面。

```vue
<!-- A.vue -->
<template>
    <button @click="sendMsg()">-</button>
</template>

<script> 
import { EventBus } from "../event-bus.js";
export default {
  methods: {
    sendMsg() {
      EventBus.$emit("aMsg", '来自A页面的消息');
    }
  }
}; 
</script>
```

接下来，我们需要在 B页面 中接收这则消息。

**接收事件**

```vue
<!-- B.vue -->
<template>
  <p>{{msgB}}</p>
</template>

<script> 
import { 
  EventBus 
} from "../event-bus.js";
export default {
  data(){
    return {
      msgB: ''
    }
  },
  mounted() {
    EventBus.$on("aMsg", (msg) => {
      // A发送来的消息
      this.msgB = msg;
    });
  }
};
</script>
```

### 坑

在上一个例子中，我们A组件向事件总线发送了一个事件aMsg并传递了参数MsgA，然后B组件对该事件进行了监听，并获取了传递过来的参数。**但是，这时如果我们离开B组件，然后再次进入B组件时，又会触发一次对事件aMsg的监听**，这时时间总线里就有两个监听了，**如果反复进入B组件多次，那么就会对aMsg进行多次的监听。**

- 解决办法：在$on组件离开，也就是被销毁前，**将该监听事件给移除**，以免下次再重复创建监听
- 语法：`this.$EventBus.$off(要移除监听的事件名)`

```js
 mounted() {
    /*调用全局Vue实例中的$EventBus事件总线中的$on属性,监听A组件发送
    到事件总线中的aMsg事件*/
    this.$EventBus.$on("aMsg", (data) => {
      //将A组件传递过来的参数data赋值给msgB
      this.msgB = data;
    });
  },
  beforeDestroy(){
    //移除监听事件"aMsg"
  	this.$EventBus.$off("aMsg")
  }
```

