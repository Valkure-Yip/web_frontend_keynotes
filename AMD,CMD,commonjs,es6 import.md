# AMD, CMD, commonjs, ES6 import

[TOC]



## Commonjs ： 

for node.js 服务端

在CommonJS中,暴露模块使用module.exports和exports，有一个全局性方法require()，用于加载模块。假定有一个数学模块math.js，就可以像下面这样加载。

```js
var math = require("math")
//调用
math.addNum(1,5) // 6
```


因为CommonJS的推动，才有了后面的AMD、CMD也采用require引用模块的风格。

### 为什么不在浏览器也是用CommonJS

CommonJS的 `require` 语法是同步的，当我们使用`require` 加载一个模块的时候，必须要等这个模块加载完后，才会执行后面的代码。**NodeJS** 是服务端，使用 `require` 语法加载模块，一般是一个文件，只需要从本地硬盘中读取文件，它的速度是比较快的。

浏览器端就不一样了，文件一般存放在服务器或者CDN上，如果使用同步的方式加载一个模块还需要由网络来决定快慢，可能时间会很长，这样浏览器很容易进入“假死状态”。所以才有了后面的**AMD**和**CMD**模块化方案，它们都是异步加载的，比较适合在浏览器端使用。

## AMD

for 客户端， 异步加载

所有依赖这个模块的语句，都定义在一个回调函数中，等到加载完成之后，这个回调函数才会运行。

定义模块
```js
define(id?,dependencies？，factory)
//id:字符串，模块名称（可选）
//dependencies:要载入的依赖模块（可选）
//factiry:工厂方法：返回一个模块函数
```
当然，如果一个模块不依赖于其他模块，那么可以直接定义在define()中
```js
//main.js
define(function (){
    var add = function(a,b){
        return a+b;
    }
    return add
})
```
如果这个模块依赖于其他模块，那它第一个函数需要是一个数组，指明它以来的模块：
```js
//main.js
define(['module1'],function (module1){
    var foo = function(){
        module1.doSomething()
    }
    return foo
})
```
当我们require()模块时，就会预先加载module1模块

require加载模块：它需要两个参数（区别于CommonJS））require([module],callback())
举个栗子：

```js
require(['math'], function (math) {
　math.add(2, 3);
});
```
math.add()和math模块异步加载，不会让浏览器发生假死现象，相对CommonJS，它更适合于浏览器。

## CMD

CMD (Common Module Definition), 是seajs推崇的规范，CMD则是**依赖就近**，用的时候再require。它写起来是这样的：
```js
define(function(require, exports, module) {
	var clock = require('clock');
	clock.start();
});
```
CMD与AMD一样，也是采用特定的define()函数来定义,用require方式来引用模块
```js
define(id?, dependencies?, factory)
//id:字符串，模块名称(可选)
//dependencies: 是我们要载入的依赖模块(可选)，使用相对路径。,注意是数组格式
//factory: 工厂方法，返回一个模块函数
define('hello', ['jquery'], function(require, exports, module) {

// 模块代码

});
```
如果一个模块不依赖其他模块，那么可以直接定义在define()函数之中。
```
define(function(require, exports, module) {
	// 模块代码
});
```
### CMD与AMD区别
AMD和CMD最大的区别是对依赖模块的执行时机处理不同，而不是加载的时机或者方式不同，二者皆为异步加载模块。
AMD依赖前置，js可以方便知道依赖模块是谁，立即加载；
而CMD就近依赖，需要使用把模块变为字符串解析一遍才知道依赖了那些模块，这也是很多人诟病CMD的一点，牺牲性能来带来开发的便利性，实际上解析模块用的时间短到可以忽略。

## Es6 Modules

Es6标准发布后，成为module标准：使用export指令导出接口，import引入模块，但是在node模块中，我们依然使用CommonJS规范，（require引入模块，module.exports导出接口）



## CommonJS 和 ES6 区别

- **CommonJS模块输出的是一个值的拷贝，ES6 模块输出的是值的引用；**
- **CommonJS 模块是运行时加载，ES6 模块是编译时输出接口。**

CommonJS输出的是值的拷贝，换句话说就是，一旦输出了某个值，如果模块内部后续的变化，影响不了外部对这个值的使用。具体例子：

```javascript
// lib.js
var counter = 3;
function incCounter() {
  counter++;
}
module.exports = {
  counter: counter,
  incCounter: incCounter,
};
```

然后我们在其它文件中使用这个模块：

```js
var mod = require('./lib');
console.log(mod.counter);  // 3，值拷贝
mod.incCounter();
console.log(mod.counter); // 3， lib.js中值变化不影响此scope
```

上面的例子充分说明了如果我们对外输出了`counter` 变量，就算后续调用模块内部的`incCounter` 方法去修改它的值，它的值依旧没有变化。

ES6模块运行机制完全不一样，JS 引擎对脚本静态分析的时候，遇到模块加载命令`import`，就会生成一个只读引用。等到脚本真正执行的时候，再根据这个只读引用，到被加载的那个模块里去取值。

```js
// lib.js
export let counter = 3;
export function incCounter() {
  counter++;
}

// main.js
import { counter, incCounter } from './lib';
console.log(counter); // 3，引用
incCounter();
console.log(counter); // 4
复制代码
```

上面代码说明，ES6 模块`import`的变量`counter`是可变的，完全反应其所在模块`lib.js`内部的变化。

而第二个差异，也是为什么ES6模块这么受人欢迎的最大原因之一。我们知道CommonJS其实加载的是一个对象，这个对象只有在脚本运行时才会生成，而且只会生成一次，这个后面我们会具体解释。但是ES6模块不是对象，它的对外接口只是一种静态定义，在代码静态解析阶段就会生成，这样我们就可以使用各种工具对JS模块进行依赖分析，优化代码，而Webpack中的 `tree shaking` 和 `scope hoisting` 实际上就是依赖ES6模块化。


### 循环加载

https://juejin.cn/post/6844904067651600391#heading-1

**CommonJS**遇到循环依赖的时候，只会输出已经执行的部分，后续的输出或者变化，是不会影响已经输出的变量。

而ES6模块相反，使用`import`加载一个变量，变量不会被缓存，真正取值的时候就能取到最终的值；

### this

在ES6模块顶层，`this`指向`undefined`；而CommonJS模块的顶层的`this`指向当前模块。其次，在ES6模块中可以直接加载CommonJS模块，但是只能整体加载，不能加载单一的输出项。

# reference

记笔记：搞清CommonJS、AMD、CMD、ES6的联系与区别_寒烟说的博客-CSDN博客_amd cmd es6 https://blog.csdn.net/hanyanshuo/article/details/110134788

CommonJS和ES6模块的区别 https://juejin.cn/post/6844904067651600391#heading-1