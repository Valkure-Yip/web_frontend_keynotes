# for...in & for...of

## for...in

### 遍历数组：

```js
Array.prototype.method = function () {
	console.log(this.length);
}
var myArray = [1, 2, 4, 5, 6, 7]
myArray.name = "数组"
for (var index in myArray) {
	// console.log(myArray[index]);
	console.log(`${index}: ${myArray[index]}`)
}
```

```
0: 1
1: 2
2: 4
3: 5
4: 6
5: 7
name: 数组
```

遍历的`index`为字符串，无法直接运算

遍历顺序有可能不是按照实际数组的内部顺序

使用for in会遍历数组所有的可枚举属性，包括原型。例如上栗的原型方法method和name属性

### 遍历对象

```js
Object.prototype.method=function(){
    console.log(this);
}
var myObject={
    a:1,
    b:2,
    c:3
}
for (var key in myObject) {
    console.log(key);
}
```

for in 可以遍历到myObject的**原型方法**method

可以在循环内部判断一下,`hasOwnPropery`方法可以判断某属性是否是该对象的实例属性

```js
for (var key in myObject) {
    if（myObject.hasOwnProperty(key)){
        console.log(key);
    }
}
```



for..of适用遍历数/数组对象/字符串/map/set等拥有迭代器对象的集合.但是不能遍历对象,**因为没有迭代器对象**.与forEach()不同的是，它可以正确响应break、continue和return语句

for-of循环不支持普通对象，但如果你想迭代一个对象的属性，你可以用for-in循环（这也是它的本职工作）或内建的Object.keys()方法



## references

作者：Haiya_32ef
链接：https://www.jianshu.com/p/c43f418d6bf0
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。