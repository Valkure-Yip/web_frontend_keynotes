# 开源协议介绍&资源



开源协议介绍：

[一文看懂开源许可证丨开源知识科普 | PingCAP](https://pingcap.com/zh/blog/introduction-of-open-source-license)

[Open Source Licenses Explained | WhiteSource](https://www.whitesourcesoftware.com/resources/blog/open-source-licenses-explained/)

OSI官网：[Licenses &amp; Standards | Open Source Initiative](https://opensource.org/licenses)

帮助选择开源协议 (**推荐**)：[Choose an open source license | Choose a License](https://choosealicense.com/)



- **Apache**：Apache 许可证（Apache License），是一个由 Apache 软件基金会发布的自由软件许可证，最初为 Apache http 服务器而撰写。Apache 许可证要求被授权者保留著作权和放弃权利的声明，但它不是一个反著作权的许可证。此许可证最新版本为 “版本2”，于 2004 年 1 月发布。Apache 许可证是宽松的，因为它不会强制派生和修改作品使用相同的许可证进行发布。
- **MIT**：MIT 许可证之名源自麻省理工学院（Massachusetts Institute of Technology, MIT），又称 “ X 条款”（X License）或 “ X11 条款”（X11 License）。MIT 内容与三条款 BSD 许可证（3-clause BSD license）内容颇为近似，但是赋予软件被授权人更大的权利与更少的限制。有许多团体均采用 MIT 许可证。例如著名的 ssh 连接软件 PuTTY 与 X Window System (X11) 即为例子。Expat 、Mono 开发平台库、Ruby on Rails、 Lua 5.0 onwards 等等也都采用 MIT 授权条款。
- **BSD**：BSD 许可协议（ Berkeley Software Distribution license ）是自由软件中使用广泛的许可协议之一。BSD 就是遵照这个许可证来发布，也因此而得名 BSD 许可协议。BSD 包最初所有者是加州大学的董事会，这是由于 BSD 源自加州大学伯克利分校。BSD 开始后，BSD 许可协议得以修正，使得以后许多 BSD 变种，都采用类似风格的条款。跟其他条款相比，从 GNU 通用公共许可证（GPL）到限制重重的著作权（Copyright），BSD 许可证比较宽松，甚至跟公有领域（Public Domain）更为接近。事实上，BSD 许可证被认为是 copycenter（中间著作权），介乎标准的 copyright 与 GPL 的 copyleft 之间。"Take it down to the copy center and make as many copies as you want"。可以说，GPL 强迫后续版本必须一样是自由软件，BSD 的后续版本可以选择要继续是 BSD 或其他自由软件条款或闭源软件等等。
- **GPL**：GPL 协议和 BSD、Apache Licence 等鼓励代码重用的许可很不一样。GPL 的出发点是代码的开源/免费使用和引用/修改/衍生代码的开源/免费使用，但不允许修改后和衍生的代码做为闭源的商业软件发布和销售。由于 GPL 严格要求使用了 GPL 类库的软件产品必须使用 GPL 协议，对于使用 GPL 协议的开源代码，商业软件或者对代码有保密要求的部门就不适合集成/采用此作为类库和二次开发的基础。
- **LGPL**：LGPL 是 GPL 的一个为主要为类库使用设计的开源协议。和 GPL 要求任何使用/修改/衍生自 GPL 类库的的软件必须采用 GPL 协议不同。LGPL 允许商业软件通过类库引用 (link) 方式使用 LGPL 类库而不需要开源商业软件的代码。这使得采用 LGPL 协议的开源代码可以被商业软件作为类库引用并发布和销售。但是如果修改 采用 LGPL 协议的代码或者对其进行衍生，则所有修改的代码，涉及修改部分的额外代码和衍生的代码都必须采用 LGPL 协议。因此采用 LGPL 协议的开源代码很适合作为第三方类库被商业软件引用，但不适合希望以采用 LGPL 协议的代码为基础，通过修改和衍生的方式做二次开发的商业软件采用。
- **SSPL**：SSPL 是 MongoDB 创建的一个源码可用的许可证，以体现开源的原则，同时提供保护，防止公有云供应商将开源作品作为服务提供而不回馈此开源作品。SSPL 允许自由和不受限制的使用和修改开源作品，但如果你把此开源作品作为服务提供给别人，你也必须在 SSPL 下公开发布任何修改以及管理层的源代码。开放源代码促进会 OSI 对 SSPL 颇有微词，它认为 SSPL 不是开源许可协议，实际上是一个源代码可用的许可证。
- **Elastic License**：Elastic License 是非商业许可证，核心条款是如果将产品作为 SaaS 使用则需要获得商业授权。