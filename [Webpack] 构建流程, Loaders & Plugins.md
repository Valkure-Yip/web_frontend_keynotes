# Webpack 构建流程，Loaders & Plugins

[TOC]



## 打包流程

> Webpack原理与实践（一）：打包流程 - 云+社区 - 腾讯云 https://cloud.tencent.com/developer/article/1409960
>
> AST抽象语法树——最基础的javascript重点知识，99%的人根本不了解 - SegmentFault 思否 https://segmentfault.com/a/1190000016231512

**入口**， 从入口开始创建依赖图

**出口**

```javascript
module.exports = {
  entry: './path/to/my/entry/file.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'my-first-webpack.bundle.js'
  }
};
```

**loader**

*loader* 让 webpack 能够去处理那些非 JavaScript 文件（webpack 自身只理解 JavaScript）

webpack 通过 *loader* 可以支持各种语言和预处理器编写模块。*loader* 描述了 webpack **如何**处理 非 JavaScript(non-JavaScript) _模块_，并且在 *bundle* 中引入这些*依赖*。

```javascript
 module: {
    rules: [
      { test: /\.txt$/, use: 'raw-loader' }
    ]
  }
```

**plugins**

只需要 `require()` 它，然后把它添加到 `plugins` 数组中

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin'); // 通过 npm 安装
const webpack = require('webpack'); // 用于访问内置插件

const config = {
  module: {
    rules: [
      { test: /\.txt$/, use: 'raw-loader' }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({template: './src/index.html'})
  ]
};

module.exports = config;
```

### 构建流程

`entry`，`loader`，`plugin`，`module`，`chunk`


`webpack` 的运行流程是一个串行的过程，从启动到结束会依次执行以下流程：

1. 初始化参数：从配置文件和 `Shell` 语句中读取与合并参数，得出最终的参数；
2. 开始编译：用上一步得到的参数初始化 `Compiler` 对象，加载所有配置的插件，执行对象的 `run` 方法开始执行编译；
3. 确定入口：根据配置中的 entry 找出所有的入口文件
4. 编译模块：从入口文件出发，调用所有配置的 Loader 对模块进行翻译，再找出该模块依赖的模块，再**递归本步骤**直到所有入口依赖的文件都经过了本步骤的处理；
5. 完成模块编译：在经过第4步使用 Loader 翻译完所有模块后，得到了每个模块被翻译后的最终内容以及它们之间的依赖关系；(**AST**: 抽象语法树)
6. 输出资源：根据入口和模块之间的依赖关系，组装成一个个包含多个模块的 `Chunk`，再把每个 `Chunk` 转换成一个单独的文件加入到输出列表，这步是可以修改输出内容的最后机会；
7. 输出完成：在确定好输出内容后，根据配置确定输出的路径和文件名，把文件内容写入到文件系统。 
8. 在以上过程中，`webpack` 会在特定的时间点广播出特定的事件，插件在监听到感兴趣的事件后会执行特定的逻辑，并且插件可以调用 `webpack` 提供的 API 改变 `webpack` 的运行结果。

![](assets/r54l7gm8qd.png)

## Loaders

Allow you to pre-process files as you `import` or “load” them. 

webpack loaders --- gulp tasks

e.g.  Loaders can transform files from a different language (like TypeScript) to JavaScript or load inline images as data URLs

Loader的特点：

- 处理一个文件可以使用多个loader，loader的执行顺序是和本身的顺序是相反的，即最后一个loader最先执行，第一个loader最后执行。
- 第一个执行的loader接收源文件内容作为参数，其他loader接收前一个执行的loader的返回值作为参数。最后执行的loader会返回此模块的JavaScript源码
- loader 可以是同步的，也可以是异步的。
- loader 是用node.js来跑，可以做一切可能的事情。
- loader 接收query参数。这些参数会传入 loaders内部作为配置来用。
- loader 可以用npm 发布或者安装。
- 除了用 package.json 的 main 导出的 loader 外， 一般的模块也可以导出一个loader。

### Install and Usage

#### 配置文件

`module.exports.module.rules = []`

e.g. css-loader & ts-loader:
`npm install --save-dev css-loader ts-loader`
为`.css`和`.ts`文件配置loader

**webpack.config.js**

```js
module.exports = {
  module: {
    rules: [
      { test: /\.css$/, use: 'css-loader' },
      { test: /\.ts$/, use: 'ts-loader' }
    ]
  }
};
```

loaders 是从最后一个开始执行（bottom to top）

### 编写一个Loader

所谓 [loader](https://webpack.docschina.org/api/loaders/) 只是一个导出为函数的 JavaScript 模块。loader runner 会调用这个函数，然后把上一个 loader 产生的结果或者资源文件(resource file)传入进去。函数的 `this` 上下文将由 webpack 填充，并且 loader runner 具有一些有用方法，可以使 loader 改变为异步调用方式，或者获取 query 参数。

第一个 loader 的传入参数只有一个：资源文件(resource file)的内容。compiler 需要得到最后一个 loader 产生的处理结果。这个处理结果应该是 `String` 或者 `Buffer`（被转换为一个 string），代表了模块的 JavaScript 源码。另外还可以传递一个可选的 SourceMap 结果（格式为 JSON 对象）。

如果是单个处理结果，可以在**同步模式**中直接返回。如果有多个处理结果，则必须调用 `this.callback()`。在**异步模式**中，必须调用 `this.async()`，来指示 loader runner 等待异步结果，它会返回 `this.callback()` 回调函数，随后 loader 必须返回 `undefined` 并且调用该回调函数

同步模式简单[示例](https://github.com/dunizb/CodeTest/tree/master/Webpack/make-loader/loaders/replaceLoader.js)

```js
const loaderUtils = require('loader-utils') // retrieving the options passed to the loader
module.exports = function(source) {
    // console.log(this.query)
    const options = loaderUtils.getOptions(this)
    const result = source.replace('hello', options.name)
    this.callback(null, result)
}
```

异步模式简单[示例](https://github.com/dunizb/CodeTest/tree/master/Webpack/make-loader/loaders/replaceLoaderAsync.js)

```js
const loaderUtils = require('loader-utils')
module.exports = function(source) {
    const options = loaderUtils.getOptions(this)
    const callback = this.async();
    setTimeout(() => {
        const result = source.replace('hello', options.name)
        callback(null, result);
    }, 1000);
}
```

webpack.config.js 使用你编写好的Loader

```js
const path = require('path')
module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    resolveLoader: {
        modules: ['node_modules', './loaders'], // 当你引用一个Loader的时候它会先去查找node_modules，如果找不到再去./loaders找
    },
    module: {
        rules: [{
            test: /\.js$/,
            use: [
                {
                    // loader: path.resolve(__dirname, './loaders/replaceLoaderAsync.js'),
                    loader: 'replaceLoaderAsync',
                    options: {
                        name: 'https'
                    }
                },
                {
                    // loader: path.resolve(__dirname, './loaders/replaceLoader.js'),
                    loader: 'replaceLoader',
                    options: {
                        name: '你好'
                    }
                }
            ]
        }]
    }
}
```



read more about writing a loader ： https://webpack.js.org/contribute/writing-a-loader/



## Plugins

webpack plugin: 一个带apply方法的JS对象（类）。webpack compiler 会调用apply方法，使其能够访问整个compilation lifecycle ？

plugin 用于扩展webpack的功能。它直接作用于 webpack，扩展了它的功能。当然loader也时变相的扩展了 webpack ，但是它只专注于转化文件（transform）这一个领域。而plugin的功能更加的丰富，而不仅局限于资源的加载。

在 Webpack 运行的**生命周期**中会广播出许多事件，**Plugin 可以监听这些事件**，在合适的时机通过 Webpack 提供的 API 改变输出结果。



e.g. 一个plugin的实现

**ConsoleLogOnBuildWebpackPlugin.js**

```javascript
const pluginName = 'ConsoleLogOnBuildWebpackPlugin';

class ConsoleLogOnBuildWebpackPlugin {
  apply(compiler) {
    compiler.hooks.run.tap(pluginName, compilation => {
      console.log('The webpack build process is starting!!!');
    });
  }
}

module.exports = ConsoleLogOnBuildWebpackPlugin;
```

compiler hook 的 tap 方法第一个参数`pluginName`是plugin的camelcase名字`'ConsoleLogOnBuildWebpackPlugin'`

`compiler.hooks.run.tap(pluginName, compilation)`

### Usage

**webpack.config.js**

`module.exports.plugins = []`

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');

module.exports = {
  entry: './path/to/my/entry/file.js',
  output: {
    filename: 'my-first-webpack.bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader'
      }
    ]
  },
  plugins: [
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({template: './src/index.html'})
  ]
};
```

###  如何编写一个Plugin

Plugin和Loader不一样，一个插件必须是一个Class，因为我们在使用插件的时候都是 `new xxxx()` 的形式。假如我们在build的时候需要创建一个版权文件，那么我们写一个Plugin来处理。

配置 webpack.config.js

```js
const path = require('path')
const CopyrightWebpackPlugin = require('./plugins/copyright-webpack-plugin')
module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    plugins: [
        new CopyrightWebpackPlugin()
    ]
}
```



定义插件：

```js
// ...
// 调用插件的时候会调用此方法
// compiler 是webpack的实例
apply(compiler) {
    // emit钩子是生成资源到 output 目录之前。异步钩子
    // compilation存放了这次打包的所有内容
    compiler.hooks.emit.tapAsync('CopyrightWebpackPlugin', (compilation, cb) => {
        // 添加copyright.txt
        compilation.assets['copyright.txt'] = {
            source: function() {
                return 'Copyright by Dunizb'
            },
            size: function() { // 文件大小,长度
                return 20;
            }
        }
        cb(); // 最后一定要调用
    })
}
```

更多[compiler.hooks](https://webpack.docschina.org/api/compiler-hooks/)，如果是同步时刻就直接调用`tap`方法。

编译结果如下，可以看到打包多出来一个 copyright.txt 文件，大小为20 bytes。并且在最终打包出来的 dist 目录下生成了一个 以“Copyright by Dunizb”为内容的名为 copyright.txt 的文件。

read more about writing a plugin:  https://webpack.js.org/contribute/writing-a-plugin/



## plugin和loader的区别是什么？

对于loader，它就是一个转换器，将A文件进行编译形成B文件，这里操作的是文件，比如将A.scss或A.less转变为B.css，单纯的文件转换过程

plugin是一个扩展器，它丰富了wepack本身，针对是loader结束后，webpack打包的整个过程，它并不直接操作文件，而是**基于事件机制工作**，会监听webpack打包过程中的某些节点，执行广泛的任务。

**loader在递归构建AST语法树过程中被调用，把其他文件转换为JS模块。**

**plugin在weback构建生命周期中，在相应事件被触发时调用**

在使用上：**loader 不需要 import 导入就可以使用，而 plugin 需要 import 导入才能使用**。



## 常见loader和plugin

**loader**：

样式：style-loader、css-loader、less-loader、sass-loader等

文件：raw-loader、file-loader 、url-loader等

编译：babel-loader、coffee-loader 、ts-loader等

校验测试：mocha-loader、jshint-loader 、eslint-loader等

**plugin**:

- HtmlWebpackPlugin: 生成 html 文件，并将打包生成的js，和css文件，插入到该html中。
- CleanWebpackPlugin:  在每次打包之前，清理dist文件夹
- UglifyJsPlugin: 压缩 js，支持文件缓存，和多线程压缩
- CommonsChunkPlugin: 提高打包效率，将第三方库和业务代码分开打包
- DefinePlugin: 允许在 编译时 创建配置的全局常量，可用于区分开发模式与生产模式
- ProvidePlugin：自动加载模块，而不必import或require它们

## references

https://webpack.js.org/concepts/loaders/

https://webpack.js.org/concepts/plugins/

https://cloud.tencent.com/developer/article/1558870

Webpack原理与实践（一）：打包流程 - 云+社区 - 腾讯云 https://cloud.tencent.com/developer/article/1409960

AST抽象语法树——最基础的javascript重点知识，99%的人根本不了解 - SegmentFault 思否 https://segmentfault.com/a/1190000016231512