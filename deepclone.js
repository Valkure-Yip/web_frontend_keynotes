function deepclone(data) {
	if (typeof data != 'object' || !data) {
		return data;
	}
	for (const key in data) {
		const element = data[key];
		// 基本数据类型的值和函数直接赋值拷贝 
		if (typeof element != 'object' || element == null) {
			obj[key] = element
		} else if (Array.isArray(element)) {
			// 实现数组的深拷贝
			obj[key] = [...element]
		} else if (element instanceof Set) {
			// 实现set的深拷贝
			obj[key] = new Set(element)
		} else if (element instanceof Map) {
			// 实现map的深拷贝
			obj[key] = new Map(element)
		} else {
			obj[key] = deepclone(element)
		}
	}
	return obj
}


// 测试数据项
var data = {
	age: 18,
	name: "liuruchao",
	education: ["小学", "初中", "高中", "大学", undefined, null],
	likesFood: new Set(["fish", "banana"]),
	friends: [
		{ name: "summer", sex: "woman" },
		{ name: "daWen", sex: "woman" },
		{ name: "yang", sex: "man" }],
	work: {
		time: "2019",
		project: { name: "test", obtain: ["css", "html", "js"] }
	},
	play: function () { console.log("玩滑板"); }
}
var res = deepclone(data)
console.log(res)