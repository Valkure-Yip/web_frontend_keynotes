# 手写new、call、apply、bind

## new

```js
function MyNew() {
    let Constructor = Array.prototype.shift.call(arguments); // 1：取出构造函数

    let obj = {} // 2：执行会创建一个新对象

    obj.__proto__ = Constructor.prototype // 3：该对象的原型等于构造函数prototype

    var result = Constructor.apply(obj, arguments) // 4： 执行函数中的代码

    return typeof result === 'object' ? result : obj // 5： 返回的值必须为对象
}
```

验证：
```js
function Man(name, age) {
    this.name = name
    this.age = age
}
var tom = new Man('tom', 20)
var mike = MyNew(Man, 'mike', 30)
console.log(tom  instanceof Man, mike instanceof Man) // true true
```

## call

- 1.判断当前`this`是否为函数，防止`Function.prototype.myCall()` 直接调用
- 2.`context` 为可选参数，如果不传的话默认上下文为 `window`
- 3.为`context` 创建一个 `Symbol`（保证不会重名）属性，将当前函数赋值给这个属性
- 4.处理参数，传入第一个参数后的其余参数
- 4.调用函数后即删除该`Symbol`属性

```js
// myCall
Function.prototype.myCall = function (context, ...args) {
	if (this === Function.prototype) {
		return undefined // 用于防止 Function.prototype.myCall() 直接调用
	}
	context = context || window;
	const fn = Symbol();
	context[fn] = this; // 向目标上下文注入这个方法
	const result = context[fn](...args); // 执行方法
	delete context[fn]; // 删除注入的方法
	return result
}
```



## apply

```js
// myApply
Function.prototype.myApply = function (context, args) {
	if (this === Function.prototype) {
		return undefined; 
	}
	context = context || window
	const fn = Symbol();
	context[fn] = this; // 向目标上下文注入这个方法
	const result = context[fn](...args);
	delete context[fn];
	return result
}
```



## bind

- 1.处理参数，返回一个闭包
- 2.判断是否为构造函数调用，如果是则使用`new`调用当前函数
- 3.如果不是，使用`apply`，将`context`和处理好的参数传入

```js
// myBind
Function.prototype.myBind = function (context,...args1) {
    if (this === Function.prototype) {
        throw new TypeError('Error')
    }
    const _this = this
    return function F(...args2) {
        // 判断是否用于构造函数
        if (this instanceof F) {
            return new _this(...args1, ...args2) // 如果构造函数是则使用new调用当前函数
        }
        return _this.apply(context, args1.concat(args2))
    }
}
```







>手动实现call、apply、bind | awesome-coding-js http://www.conardli.top/docs/JavaScript/%E6%89%8B%E5%8A%A8%E5%AE%9E%E7%8E%B0call%E3%80%81apply%E3%80%81bind.html#%E6%A8%A1%E6%8B%9F%E5%AE%9E%E7%8E%B0call
>
>new,call,apply,bind方法的实现原理 - SegmentFault 思否 https://segmentfault.com/a/1190000021905571