function shuffle_Fisher_Yates(arr){
	var m = arr.length;
	while(m > 1){
		let index = Math.floor(Math.random()*m);
		m--;
		[arr[m] , arr[index]] = [arr[index] , arr[m]]
	}
	return arr;
}

var arr = [1,2,3,4,5,6,7,8,9,10]
console.log(shuffle_Fisher_Yates(arr))