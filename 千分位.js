/**
 * 
 * @param {number} num 
 */
function parseNum(num){
    var numStr = num.toString();
    var res = ''
    while(true){
        if(numStr.length <= 3){
            res = numStr + res;
            break;
        } else {
            res = ',' + numStr.slice(numStr.length-3) + res
            numStr = numStr.substr(0, numStr.length-3)
        }
    }
    return res
}

console.log(parseNum(432532451234567890))