# CSS preprocessor VS post-processor



[Deconfusing Pre- and Post-processing | by Stefan Baumgartner | Medium](https://medium.com/@ddprrt/deconfusing-pre-and-post-processing-d68e3bd078a3)

### pre-processing VS post-processing

**Preprocessor**: 

Pre-processing always involved another language. Sass, LESS, Stylus. You name it. The language’s name was also the name of the tool. And you wrote in that language to process it to CSS. This coined the term pre-processor: First it’s something different, then it’s CSS.

**Postprocessor**: 

Post-processing on the other hand happened after we already had CSS in place. Post-processors did the heavy lifting for us. They made changes to our code, which we didn’t want to care about: Applying vendor prefixes automatically. Creating pixel fallbacks for every “rem” unit that we used. And extracting all the mobile first media query content for a certain viewport to give IE8 something nice to present.

>  Pre-processing was all about crafting and things that CSS couldn’t do (extensions). Post-processing was about optimisations and automation.



![img](/Users/zhitong.ye/Desktop/开发技术笔记/web_frontend_keynotes/CSS preprocessor vs post-processor.assets/1*ZMX0_2_gQ0hyPy0J_0X9BA.jpeg)



postCSS的出现大大增强了后处理的能力，一些原来只有sass等preprocess能处理的特性（variables, nesting, mixins）也可以由postCSS搞定，但这些特性严格来说不是CSS标准一部分，这些代码也不会出现在最终的CSS中，所以postCSS的这些功能严格来说已经不算post-processing了

同样的sass等提供的Automatic imports and minification也是对生成后的css做处理，严格来说这也应归为后处理

> Pre-processing 和 post-processing的边界随着技术发展越发模糊



### 更准确的讲法：authoring & optimization

In the end, both tools are CSS processors. Helping you to get stuff done.

![img](/Users/zhitong.ye/Desktop/开发技术笔记/web_frontend_keynotes/CSS preprocessor vs post-processor.assets/1*v-BSZh85N3kHdo2tbTHMug.jpeg)

Figure 5: CSS Processors. Including authoring aspects and optimisation features. While PostCSS can cover all optimisation features, you can add one further optimisation step after the original Sass run