/**
 * Tricks of the trade: Recursion to Iteration, Part 1: The Simple Method, secret features, and accumulators - Tom Moertel’s Blog https://blog.moertel.com/posts/2013-05-11-recursive-to-iterative.html
 */

function factorial_recursion(n){
	if (n<2){
		return 1
	}
	return n*factorial_recursion(n-1)
}

function factorial_recursion_2(n, acc=1){
	while(n > 1){
		acc = acc*n;
		n = n - 1
	}
	return acc
	
}