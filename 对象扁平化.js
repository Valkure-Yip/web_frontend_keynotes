function flat(obj){
	var res = {};
	for (const key in obj) {
		if (Object.hasOwnProperty.call(obj, key)) {
			if (typeof obj[key] != 'object'){
				res[key] =  obj[key]
			} else if (Array.isArray(obj[key])){
				for (let i = 0; i < obj[key].length; i++) {
					res[`${key}[${i}]`] = obj[key][i];
				}
			} else {
				let parentKey = key;
				let childObj = flat(obj[key]);
				for (const key in childObj) {
					if (Object.hasOwnProperty.call(childObj, key)) {
						res[parentKey+'.'+key] =  childObj[key]
					}
				}
			}
			 
		}
	}
	return res
}

var obj = { a: {b: {c: { d: 1 }}}, aa: 2, c: [1, 2] }
console.log(JSON.stringify(flat(obj)))