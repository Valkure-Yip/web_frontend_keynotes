# Prism: OpenAPI mocker



`"mock": "prism mock ./src/api/api-docs.json"`

动态生成response [HTTP Mocking | Prism](https://meta.stoplight.io/docs/prism/ZG9jOjk1-http-mocking#static-or-dynamic-generation)：

`"mock": "prism mock -d ./src/api/api-docs.json"`

配合`x-faker`控制生成的随机内容的具体类型（https://meta.stoplight.io/docs/prism/ZG9jOjk1-http-mocking#dynamic-response-generation）



### docs

https://meta.stoplight.io/docs/prism/ZG9jOjk1-http-mocking#response-generation

x-faker：http://marak.github.io/faker.js/



## 遇到的坑

**401 unauthorized： **

删掉接口定义中`security`字段：

```json
"security": [
  {
    "oauth2": [
      "read",
      "trust",
      "write"
    ]
  }
],
```



**500 :[NO_COMPLEX_OBJECT_TEXT](https://meta.stoplight.io/docs/prism/ZG9jOjE2MDY1NjY5-errors#no_complex_object_text)  - Message: Cannot serialise complex objects as text:** 

对`"produces": ["*/*"],`适配有问题， 替换成`application/json`就行



