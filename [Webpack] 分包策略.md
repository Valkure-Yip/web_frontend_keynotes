# [Webpack] 分包策略

[TOC]

## 默认分包策略

在 webpack 打包过程中，经常出现 `vendor.js`， `app.js` 单个文件较大的情况，这偏偏又是网页最先加载的文件，这就会使得加载时间过长，从而使得白屏时间过长，影响用户体验。所以我们需要有合理的分包策略。

**CommonsChunkPlugin**

在 Webapck4.x 版本之前，我们都是使用 `CommonsChunkPlugin` 去做分离

```javascript
plugins: [
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: function (module, count) {
      return (
        module.resource &&
        /.js$/.test(module.resource) &&
        module.resource.indexOf(path.join(__dirname, './node_modules')) === 0
      )
    },
  }),
  new webpack.optimize.CommonsChunkPlugin({
    name: 'common',
    chunks: 'initial',
    minChunks: 2,
  }),
]
```

我们把以下文件单独抽离出来打包

- `node_modules` 文件夹下的，模块
- 被 3 个 入口 `chunk` 共享的模块

**optimization.splitChunks**

webpack 4 最大的改动就是废除了 `CommonsChunkPlugin` 引入了 `optimization.splitChunks`。如果你的 `mode` 是 `production`，那么 webpack4 就会自动开启 `Code Splitting`。

它内置的代码分割策略是这样的：

- 新的 chunk 是否被共享或者是来自 `node_modules` 的模块
- 新的 chunk 体积在压缩之前是否大于 30kb
- 按需加载 chunk 的并发请求数量小于等于 5 个
- 页面初始加载时的并发请求数量小于等于 3 个

虽然在 webpack4 会自动开启 `Code Splitting`，但是随着项目工程的最大，这往往不能满足我们的需求，我们需要再进行个性化的优化。

## 优化分包策略

拆包策略：

- 基础类库 `chunk-libs`
- UI 组件库 `chunk-elementUI`
- 自定义共用组件/函数 `chunk-commons`
- 低频组件 `chunk-eachrts`/`chunk-xlsx`等
- 业务代码 lazy-loading `xxxx.js`

e.g. :

假设有一个基于Vue和Element-UI的管理后台项目**按默认策略**打包后如下：

（使用可视化插件：webpack-bundle-analyzer）

![img](https://user-gold-cdn.xitu.io/2018/7/24/164cac10a2222794?w=3348&h=1880&f=jpeg&s=720643)

如上图所示，由于`element-ui`在`entry`入口文件中被引入并且被大量页面共用，所以它默认会被打包到 `app.js` 之中。这样做是**不合理的**，因为`app.js`里还含有你的`router 路由声明`、`store 全局状态`、`utils 公共函数`，`icons 图标`等等这些全局共用的东西。`element-ui`不会经常变化，但业务逻辑代码会经常修改。这样再次打包时`app.xxx.js`时**文件名hash会变化**，导致`element-ui`和 `vue/react`等代码和业务逻辑代码的**缓存一并失效**

我们现在的策略是按照**体积大小、共用率、更新频率**重新划分我们的包，使其**尽可能的利用浏览器缓存**。

| 类型            | 共用率 | 使用频率 | 更新频率 | 例子                                                         |
| :-------------- | :----: | :------: | :------: | ------------------------------------------------------------ |
| 基础类库        |   高   |    高    |    低    | vue/react、vuex/mobx、xx-router、axios 等                    |
| UI 组件库       |   高   |    高    |    中    | Element-UI/Ant Design 等                                     |
| 必要组件/函数   |   高   |    高    |    中    | Nav/Header/Footer 组件、路由定义、权限验证、全局 State 、全局配置等 |
| 非必要组件/函数 |   高   |    高    |    中    | 封装的 Select/Radio 组件、utils 函数 等 (必要和非必要组件可合并) |
| 低频组件        |   低   |    低    |    低    | 富文本、Mardown-Editor、Echarts、Dropzone 等                 |
| 业务代码        |   低   |    高    |    高    | 业务组件、业务模块、业务页面  等                             |

- **基础类库** chunk-libs

比如 `vue`+`vue-router`+`vuex`+`axios` 这种标准的全家桶，**升级频率都不高，但每个页面都需要它们**。（一些全局被共用的，体积不大的第三方库也可以放在其中：比如 nprogress、js-cookie、clipboard 等）

- **UI 组件库**

理论上 UI 组件库也可以放入 libs 中，但这里单独拿出来的原因是： 它实在是比较大，不管是 `Element-UI`还是`Ant Design` gizp 压缩完都可能要 200kb 左右，它可能比 libs 里面所有的库加起来还要大不少，而且 UI 组件库的更新频率也相对的比 libs 要更高一点。我们不时的会升级 UI 组件库来解决一些现有的 bugs 或使用它的一些新功能。所以建议将 UI 组件库也单独拆成一个包。

- **自定义组件/函数** chunk-commons

这里的 commons 主要分为 **必要**和**非必要**。

**必要组件**：项目里必须加载它们才能正常运行的组件或者函数。比如你的路由表、全局 state、全局侧边栏/Header/Footer 等组件、自定义 Svg 图标等等。这些其实就是你在入口文件中依赖的东西，它们都会默认打包到`app.js`中。

**非必要组件**：被大部分页面使用，但在入口文件 entry 中未被引入的模块。比如：一个管理后台，你封装了很多 select 或者 table 组件，由于它们的体积不会很大，它们都会被默认打包到到每一个懒加载页面的 chunk 中，这样会造成不少的浪费。你有十个页面引用了它，就会包重复打包十次。所以应该将那些被大量共用的组件单独打包成`chunk-commons`。

不过还是要结合具体情况来看。一般情况下，你也可以将那些*非必要组件\函数*也在入口文件 entry 中引入，和*必要组件\函数*一同打包到`app.js`之中也是没什么问题的。

- **低频组件**

低频组件和上面的共用组件 `chunk-commons` 最大的区别是，它们只会在一些特定业务场景下使用，比如富文本编辑器、`js-xlsx`前端 excel 处理库等。一般这些库都是第三方的且大于 30kb，所以 webpack 4 会默认打包成一个独立的 bundle。也无需特别处理。小于 30kb 的情况下会被打包到具体使用它的页面 bundle 中。

- **业务代码**

这部分就是我们平时经常写的业务代码。一般都是按照页面的划分来打包，比如在 vue 中，使用[路由懒加载](https://router.vuejs.org/zh/guide/advanced/lazy-loading.html)的方式加载页面 `component: () => import('./Foo.vue')` webpack 默认会将它打包成一个独立的 bundle。

完整配置代码：

```js
splitChunks: {
  chunks: "all",
  cacheGroups: {
    libs: {
      name: "chunk-libs",
      test: /[\\/]node_modules[\\/]/,
      priority: 10,
      chunks: "initial" // 只打包初始时依赖的第三方
    },
    elementUI: {
      name: "chunk-elementUI", // 单独将 elementUI 拆包
      priority: 20, // 权重要大于 libs 和 app 不然会被打包进 libs 或者 app
      test: /[\\/]node_modules[\\/]element-ui[\\/]/
    },
    commons: {
      name: "chunk-comomns",
      test: resolve("src/components"), // 可自定义拓展你的规则
      minChunks: 2, // 最小共用次数
      priority: 5,
      reuseExistingChunk: true
    }
  }
};
```



最终拆包效果：**尽可能的利用了浏览器缓存**

![img](https://user-gold-cdn.xitu.io/2018/8/3/164fefcc3a2ef69f?w=1872&h=1390&f=jpeg&s=383822)

## References

默认分包策略 | awesome-bookmarks https://panjiachen.github.io/awesome-bookmarks/blog/webpack/webpack4-b.html

Webpack的异步加载原理及分包策略(深度好文，建议收藏) - 云+社区 - 腾讯云 https://cloud.tencent.com/developer/article/1756929