# mock service worker

[Introduction - Mock Service Worker Docs](https://mswjs.io/docs/)

Mock Service Worker is an API mocking library that uses Service Worker API to intercept actual requests.

```bash
npm install msw --save-dev
# or
yarn add msw --dev
```

