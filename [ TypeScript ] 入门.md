# TypeScript

[TOC]



`npm install -g typescript`

将ts编译成js

`tsc hello.ts` ---> `hello.js`

### 声明类型

基础类型：`string`, `boolean`, `number`, `any`

**泛型**：`Array<number>`



### Type Annotations on Variables

When you declare a variable using `const`, `var`, or `let`, you can optionally add a type annotation to explicitly specify the type of the variable ( ts 也可以自行推断 ):

```ts
let myName: string = "Alice";
```



### Functions

参数列表类型

```ts
function greet(person: string, date: Date) {
  console.log(`Hello ${person}, today is ${date.toDateString()}!`);
}


```

返回值类型

```ts
function getFavoriteNumber(): number {
  return 26;
}
```

表示函数结构：类似箭头函数写法

```ts
function greeter(fn: (a: string) => void) {
  fn("Hello, World");
}
```

或者

```ts
type GreetFunction = (a: string) => void;
function greeter(fn: GreetFunction) {
  // ...
}
```

### Object

```ts
// The parameter's type annotation is an object type
function printCoord(pt: { x: number; y: number }) {
  console.log("The coordinate's x value is " + pt.x);
  console.log("The coordinate's y value is " + pt.y);
}
printCoord({ x: 3, y: 7 });
```

Optional property:  `?`

```ts
function printName(obj: { first: string; last?: string }) {
  // ...
}
```



### Union Types

TypeScript will only allow you to do things with the union if that thing is valid for *every* member of the union. For example, if you have the union `string | number`, you can’t use methods that are only available on `string`:

```ts
function printId(id: number | string) {
  console.log(id.toUpperCase());
	// Property 'toUpperCase' does not exist on type 'string | number'.
  // Property 'toUpperCase' does not exist on type 'number'.
}
```

Solution: **narrowing** down types



### Type alias

A *type alias* is exactly that - a *name* for any *type*. The syntax for a type alias is:

```ts
type Point = {
  x: number;
  y: number;
};

type ID = number | string;
```



### Interfaces

An *interface declaration* is another way to name an object type:

```ts
interface Point {
  x: number;
  y: number;
}

function printCoord(pt: Point) {
  console.log("The coordinate's x value is " + pt.x);
  console.log("The coordinate's y value is " + pt.y);
}

printCoord({ x: 100, y: 100 });
```



#### Difference between interface and type

The key distinction is that a type cannot be re-opened to add new properties vs an interface which is always extendable.



![image-20210722122111483](/Users/zhitong.ye/Desktop/开发技术笔记/web_frontend_keynotes/[ TypeScript ] 入门.assets/image-20210722122111483.png)



### Type assertion

```ts
const myCanvas = document.getElementById("main_canvas") as HTMLCanvasElement;
// or
const myCanvas = <HTMLCanvasElement>document.getElementById("main_canvas");
```

TypeScript only allows type assertions which convert to a ***more specific* or *less specific*** version of a type. This rule prevents “impossible” coercions like:

```ts
const x = "hello" as number;
// Conversion of type 'string' to type 'number' may be a mistake because neither type sufficiently overlaps with the other. If this was intentional, convert the expression to 'unknown' first.Conversion of type 'string' to type 'number' may be a mistake because neither type sufficiently overlaps with the other. If this was intentional, convert the expression to 'unknown' first.
```

**Workaround**:

Sometimes this rule can be too conservative and will disallow more complex coercions that might be valid. If this happens, you can use two assertions, first to `any` (or `unknown`, which we’ll introduce later), then to the desired type:

```ts
const a = (expr as any) as T;
```



### `unknown`

The `unknown` type represents *any* value. This is similar to the `any` type, but is safer because it’s not legal to do anything with an `unknown` value:

```
function f1(a: any) {
  a.b(); // OK
}
function f2(a: unknown) {
  a.b();Object is of type 'unknown'.Object is of type 'unknown'.
}
```

This is useful when describing function types because you can describe functions that accept any value without having `any` values in your function body.

Conversely, you can describe a function that returns a value of unknown type:

```
function safeParse(s: string): unknown {
  return JSON.parse(s);
}
 
// Need to be careful with 'obj'!
const obj = safeParse(someRandomString);
```



### Literal as Types

```ts
function printText(s: string, alignment: "left" | "right" | "center") {
  // ...
}
printText("Hello, world", "left");
printText("G'day, mate", "centre"); // error!!
```

```ts
function compare(a: string, b: string): -1 | 0 | 1 {
  return a === b ? 0 : a > b ? 1 : -1;
}
```

```ts
function configure(x: Options | "auto") {
  // ...
}
```



### Non-null Assertion Operator (Postfix`!`)

TypeScript also has a special syntax for removing `null` and `undefined` from a type without doing any explicit checking. Writing `!` after any expression is effectively a type assertion that the value isn’t `null` or `undefined`:

```
function liveDangerously(x?: number | null) {
  // No error
  console.log(x!.toFixed());
}Try
```



### Discrimiated Unions

```ts
interface Circle {
  kind: "circle";
  radius: number;
}

interface Square {
  kind: "square";
  sideLength: number;
}

type Shape = Circle | Square;

function getArea(shape: Shape) {
  if (shape.kind === "circle") {
    return Math.PI * shape.radius ** 2;
                      
(parameter) shape: Circle
  }
}
```

When every type in a union contains a common property with literal types, TypeScript considers that to be a *discriminated union*, and can narrow out the members of the union.

In this case, `kind` was that common property (which is what’s considered a *discriminant* property of `Shape`). Checking whether the `kind` property was `"circle"` got rid of every type in `Shape` that didn’t have a `kind` property with the type `"circle"`. That narrowed `shape` down to the type `Circle`.



### Generics

*generics* are used when we want to describe a **correspondence between two values**. We do this by declaring a *type parameter* in the function signature:

```ts
function firstElement<Type>(arr: Type[]): Type {
  return arr[0];
}
```

By adding a type parameter `Type` to this function and using it in two places, we’ve created a link between the input of the function (the array) and the output (the return value). Now when we call it, a more specific type comes out:

```ts
// s is of type 'string'
const s = firstElement(["a", "b", "c"]);
// n is of type 'number'
const n = firstElement([1, 2, 3]);
```



#### Inference

Note that we didn’t have to specify `Type` in this sample. The type was ***inferred*** - chosen automatically - by TypeScript.

We can use multiple type parameters as well. For example, a standalone version of `map` would look like this:

```ts
function map<Input, Output>(arr: Input[], func: (arg: Input) => Output): Output[] {
  return arr.map(func);
}

// Parameter 'n' is of type 'string'
// 'parsed' is of type 'number[]'
const parsed = map(["1", "2", "3"], (n) => parseInt(n));
```



#### constraints: `<T extends ...>`

e.g. we need `<Type>` to have a `length` property that’s a number.

```ts
function longest<Type extends { length: number }>(a: Type, b: Type) {
  if (a.length >= b.length) {
    return a;
  } else {
    return b;
  }
}

// longerArray is of type 'number[]'
const longerArray = longest([1, 2], [1, 2, 3]);
// longerString is of type 'string'
const longerString = longest("alice", "bob");
// Error! Numbers don't have a 'length' property
const notOK = longest(10, 100);
```



### Optional param & default value

```ts
function f(x?: number) {
  // ...
}

function f(x = 10) {
  // ...
}
```

注意**回调函数**可选参数写法：https://www.typescriptlang.org/docs/handbook/2/functions.html#optional-parameters-in-callbacks



### Function Overloads

In this example, we wrote two overloads: one accepting one argument, and another accepting three arguments. These first two signatures are called the *overload signatures*.

Then, we wrote a function implementation with a compatible signature. Functions have an *implementation* signature, but this signature can’t be called directly. Even though we wrote a function with two optional parameters after the required one, it can’t be called with two parameters!

```ts
// line 1 & 2: overload signature
// line 3+: implementation
function makeDate(timestamp: number): Date;
function makeDate(m: number, d: number, y: number): Date;
function makeDate(mOrTimestamp: number, d?: number, y?: number): Date {
  if (d !== undefined && y !== undefined) {
    return new Date(y, mOrTimestamp, d);
  } else {
    return new Date(mOrTimestamp);
  }
}
const d1 = makeDate(12345678);
const d2 = makeDate(5, 5, 5);
const d3 = makeDate(1, 3);

// No overload expects 2 arguments, but overloads do exist that expect either 1 or 3 arguments
```



> Always prefer parameters with union types instead of overloads when possible



### `this` in a function

TypeScript uses *`this` in parameter* to let you declare the type for `this` in the function body.

```ts
interface DB {
  filterUsers(filter: (this: User) => boolean): User[];
}

const db = getDB();
const admins = db.filterUsers(function (this: User) {
  return this.admin;
});
```

This pattern is common with callback-style APIs, where another object typically controls when your function is called. Note that you need to use `function` and not arrow functions to get this behavior:







### keyof, typeof & loop through object

`keyof` takes an object type and returns a type that accepts any of the object's keys.

```js
type Point = { x: number; y: number };
type P = keyof Point; // type '"x" || "y"'

const coordinate: P = 'z' // Type '"z"' is not assignable to type '"x" | "y"'.
```

#### typeof with TypeScript types

`typeof` behaves differently when called on javascript objects, to when it is called on typescript types.

- TypeScript uses [javascript's typeof](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof) when called on javascript values at runtime and returns one of `"undefined", "object", "boolean", "number", "bigint", "string", "symbol", "function"`
- [TypeScript's typeof](https://www.typescriptlang.org/docs/handbook/2/typeof-types.html) is called on type values, but can also be called on javascript values when in a type expression. It can also infer the type of javascript objects, returning a more detailed object type.

```js
type Language = 'EN' | 'ES'; 
const userLanguage: Language = 'EN';
const preferences = { language: userLanguage, theme: 'light' };

console.log(typeof preferences); // "object"
type Preferences = typeof preferences; // type '{language: 'EN''; theme: string; }'
```

Because the second `typeof preferences` is in a type expression it is actually TypeScript's own `typeof` that get called, and not javascript's.

#### keyof typeof

Because [`keyof`](https://www.typescriptlang.org/docs/handbook/2/keyof-types.html) is a TypeScript concept we will be calling TypeScript's verion of `typeof`.

`keyof typeof` will infer the type of a javascript object and return a type that is the union of its keys. Because it can infer the exact value of the keys it can return a union of their [literal types](https://www.typescriptlang.org/docs/handbook/literal-types.html) instead of just returning "string".

```js
type PreferenceKeys = keyof typeof preferences; // type '"language" | "theme"'
```

#### loop through object

```tsx
Object.keys(JiraStatus).map((key) => (
  // 注意这里使用keyof typeof, 否则ts报错index类型不对
  <Select.Option value={JiraStatus[key as keyof typeof JiraStatus]}>{key}</Select.Option>
))
```

或者使用ES2017的`Object.entries` 不用担心类型问题

```tsx
Object.entries(JiraStatus).map(([key, val]) => (
  <Select.Option value={val}>{key}</Select.Option>
))
```



### .d.ts 类型声明文件

typescript 解析器会解析项目中所有`d.ts`文件，其中所有定义均对全局有效

Guide [声明文件 · TypeScript 入门教程](https://ts.xcatliu.com/basics/declaration-files.html#%E6%96%B0%E8%AF%AD%E6%B3%95%E7%B4%A2%E5%BC%95)

Offical doc [TypeScript: Documentation - Declaration Reference](https://www.typescriptlang.org/docs/handbook/declaration-files/by-example.html)
