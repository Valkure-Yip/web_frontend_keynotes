# 防抖&节流

## 防抖

![image-20210415113534045](assets/image-20210415113534045.png)

```js
var debounce = function(fn, waitTime){
    var timeout = null;
    return function(...args){
        clearTimeout(timeout)
        timeout = setTimeout(()=>{fn.apply(this, args)}, waitTime)
    }
}
```



## 节流

![image-20210415113610639](assets/image-20210415113610639.png)

```js
var throttle = function(fn, waitTime){
    var prev = 0;
    return function(...args){
        var now = Date.now();
        if (now-prev > waitTime){
            fn.apply(this, args);
            prev = now
        }
    }
}
```

