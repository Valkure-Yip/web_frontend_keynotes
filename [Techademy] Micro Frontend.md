# Techademy: Micro Frontend



host

Micro app

slots



using slots instead of importing components, so that they don't get bundled together and build together

configure what slots to be iincluded in SPACE



```jsx
// index.js
<slot slotName="..." slotProps={...}>
```



```jsx
// slot.js
useEffect(()=>{
	// load module codes on the run
})
```



[How to Develop Microfrontends Using React: Step by Step Guide | by Rumesh Eranga Hapuarachchi | Bits and Pieces](https://blog.bitsrc.io/how-to-develop-microfrontends-using-react-step-by-step-guide-47ebb479cacd)

