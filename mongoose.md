# Mongoose.js

```
schema --(mongoose.model())--> model --(new Model())--> document
```



## schemas

Each schema maps to a MongoDB collection and defines the shape of the documents within that collection.

```javascript
 import mongoose from 'mongoose';
  const { Schema } = mongoose;

  const blogSchema = new Schema({
    title:  String, // String is shorthand for {type: String}
    author: String,
    body:   String,
    comments: [{ body: String, date: Date }],
    date: { type: Date, default: Date.now },
    hidden: Boolean,
    meta: {
      votes: Number,
      favs:  Number
    }
  });
```

The permitted SchemaTypes are:

- [String](https://mongoosejs.com/docs/schematypes.html#strings)
- [Number](https://mongoosejs.com/docs/schematypes.html#numbers)
- [Date](https://mongoosejs.com/docs/schematypes.html#dates)
- [Buffer](https://mongoosejs.com/docs/schematypes.html#buffers)
- [Boolean](https://mongoosejs.com/docs/schematypes.html#booleans)
- [Mixed](https://mongoosejs.com/docs/schematypes.html#mixed)
- [ObjectId](https://mongoosejs.com/docs/schematypes.html#objectids)
- [Array](https://mongoosejs.com/docs/schematypes.html#arrays)
- [Decimal128](https://mongoosejs.com/docs/api.html#mongoose_Mongoose-Decimal128)
- [Map](https://mongoosejs.com/docs/schematypes.html#maps)

Schemas not only define the structure of your document and casting of properties, they also define document [instance methods](https://mongoosejs.com/docs/guide.html#methods), [static Model methods](https://mongoosejs.com/docs/guide.html#statics), [compound indexes](https://mongoosejs.com/docs/guide.html#indexes), and document lifecycle hooks called [middleware](https://mongoosejs.com/docs/middleware.html).

### model

```javascript
 const Blog = mongoose.model('Blog', blogSchema);
  // ready to go!
```

### [Ids](https://mongoosejs.com/docs/guide.html#_id)

By default, Mongoose adds an `_id` property to your schemas.

```javascript
const schema = new Schema();

schema.path('_id'); // ObjectId { ... }
```

When you create a new document with the automatically added `_id` property, Mongoose creates a new [`_id` of type ObjectId](https://masteringjs.io/tutorials/mongoose/objectid) to your document.

```javascript
const Model = mongoose.model('Test', schema);

const doc = new Model();
doc._id instanceof mongoose.Types.ObjectId; // true
```

### [Instance methods](https://mongoosejs.com/docs/guide.html#methods)

Instances of `Models` are [documents](https://mongoosejs.com/docs/documents.html). Documents have many of their own [built-in instance methods](https://mongoosejs.com/docs/api/document.html). We may also define our own custom document instance methods.

```javascript
  // define a schema
  const animalSchema = new Schema({ name: String, type: String });

  // assign a function to the "methods" object of our animalSchema
  animalSchema.methods.findSimilarTypes = function(cb) {
    return mongoose.model('Animal').find({ type: this.type }, cb);
  };
```

Now all of our `animal` instances have a `findSimilarTypes` method available to them.

```javascript
  const Animal = mongoose.model('Animal', animalSchema);
  const dog = new Animal({ type: 'dog' });

  dog.findSimilarTypes((err, dogs) => {
    console.log(dogs); // woof
  });
```

- Overwriting a default mongoose document method may lead to unpredictable results. See [this](https://mongoosejs.com/docs/api.html#schema_Schema.reserved) for more details.
- The example above uses the `Schema.methods` object directly to save an instance method. You can also use the `Schema.method()` helper as described [here](https://mongoosejs.com/docs/api.html#schema_Schema-method).
- Do **not** declare methods using ES6 arrow functions (`=>`). Arrow functions [explicitly prevent binding `this`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions#No_binding_of_this), so your method will **not** have access to the document and the above examples will not work.

### [Statics](https://mongoosejs.com/docs/guide.html#statics)

You can also add static functions to your model. There are two equivalent ways to add a static:

- Add a function property to `schema.statics`
- Call the [`Schema#static()` function](https://mongoosejs.com/docs/api.html#schema_Schema-static)

```javascript
  // Assign a function to the "statics" object of our animalSchema
  animalSchema.statics.findByName = function(name) {
    return this.find({ name: new RegExp(name, 'i') });
  };
  // Or, equivalently, you can call `animalSchema.static()`.
  animalSchema.static('findByBreed', function(breed) { return this.find({ breed }); });

  const Animal = mongoose.model('Animal', animalSchema);
  let animals = await Animal.findByName('fido');
  animals = animals.concat(await Animal.findByBreed('Poodle'));
```

Do **not** declare statics using ES6 arrow functions (`=>`).

### [Query Helpers](https://mongoosejs.com/docs/guide.html#query-helpers)

You can also add query helper functions, which are like instance methods but for mongoose queries. Query helper methods let you extend mongoose's [chainable query builder API](https://mongoosejs.com/docs/queries.html).

```javascript
  animalSchema.query.byName = function(name) {
    return this.where({ name: new RegExp(name, 'i') })
  };

  const Animal = mongoose.model('Animal', animalSchema);

  Animal.find().byName('fido').exec((err, animals) => {
    console.log(animals);
  });

  Animal.findOne().byName('fido').exec((err, animal) => {
    console.log(animal);
  });
```

### [Virtuals](https://mongoosejs.com/docs/guide.html#virtuals)

[Virtuals](https://mongoosejs.com/docs/api.html#schema_Schema-virtual) are document properties that you can get and set but that do not get persisted to MongoDB. The getters are useful for formatting or combining fields, while setters are useful for de-composing a single value into multiple values for storage.

```javascript
 // define a schema
  const personSchema = new Schema({
    name: {
      first: String,
      last: String
    }
  });

  // compile our model
  const Person = mongoose.model('Person', personSchema);

  // create a document
  const axl = new Person({
    name: { first: 'Axl', last: 'Rose' }
  });	
```

```js
// virtual
  personSchema.virtual('fullName').
  get(function() {
    return this.name.first + ' ' + this.name.last;
    }).
  set(function(v) {
    this.name.first = v.substr(0, v.indexOf(' '));
    this.name.last = v.substr(v.indexOf(' ') + 1);
  });

	axl.fullName = 'William Rose'; // Now `axl.name.first` is "William"
```



## [SchemaTypes](https://mongoosejs.com/docs/schematypes.html#schematypes)

You can think of a Mongoose schema as the configuration object for a Mongoose model. A SchemaType is then a configuration object for an individual property. 

## 