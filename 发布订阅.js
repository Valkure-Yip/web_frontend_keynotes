class EventEmitter {
	constructor(){
		this.events = {};
	}
	// 订阅事件
	on(eventName, cb){
		if (!this.events[eventName]){
			this.events[eventName] = [cb]
		} else {
			this.events[eventName].push(cb)
		}
	}
	// 发布事件
	emit(eventName){
		this.events[eventName] && this.events[eventName].forEach(cb=>cb());
	}
	// 取消订阅
	removeListener(eventName, callback){
		this.events[eventName] = this.events[eventName].filter(cb => cb != callback);
	}
	// 订阅一次
	once(eventName, cb){
		let fn = ()=>{
			cb();
			this.removeListener(eventName, fn)
		}
		this.on(eventName, fn)
	}
}

let em = new EventEmitter();
let workday = 0;
em.on("work", function() {
    workday++;
    console.log("work everyday");
});

em.once("love", function() {
    console.log("just love you");
});

function makeMoney() {
    console.log("make one million money");
}
em.on("money",makeMoney);

let time = setInterval(() => {
    em.emit("work");
    em.removeListener("money",makeMoney);
    em.emit("money");
    em.emit("love");
    if (workday === 5) {
        console.log("have a rest")
        clearInterval(time);
    }
}, 1);