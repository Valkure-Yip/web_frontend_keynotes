Promise.myAll = function (promises) {
	let results = [];
	let promiseCount = 0;
	let promisesLength = promises.length;
	return new Promise(function (resolve, reject) {
		for (let i = 0; i < promises.length; i++) {
			// 将val作为resolve参数，以防有些val不是promise类型
			Promise.resolve(promises[i]).then(function (res) {
				promiseCount++;
				results[i] = res;
				// 当所有函数都正确执行了，resolve输出所有返回结果。
				if (promiseCount === promisesLength) {
					return resolve(results);
				}
			}, function (err) {
				return reject(err);
			});
		}
	});
};

Promise.myRace = function(promises){
	let hasValue = false;
	let hasError = false;
	return new Promise(function (resolve, reject) {
		for (let i = 0; i < promises.length; i++) {
			Promise.resolve(promises[i]).then(data=>{
				!hasValue && !hasError && resolve(data)
				hasValue = true
			}, error=>{
				!hasValue && !hasError && reject(error)
				hasError = true
			})
		}
	});
}

// test
var p1 = new Promise((resolve, reject)=>{
	setTimeout(()=>{resolve(3)},0)
})
var p2  = Promise.resolve("promise2");
var p3 = Promise.resolve("promise3")
// promise.all
// Promise.myAll([p1, p2]).then((res)=>{
// 	console.log(res)
// })
// native
// Promise.all([p1, p2]).then((res)=>{
// 	console.log(res)
// })

// promise.race
Promise.race([p2,p3]).then((res)=>{
	console.log(res)
})
Promise.myRace([p2,p3]).then((res)=>{
	console.log(res)
})
