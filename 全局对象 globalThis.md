# 全局对象 & globalThis



## globalThis： 跨平台的全局对象引用

在以前，从不同的 JavaScript 环境中获取全局对象需要不同的语句。在 Web 中，可以通过 `window`、`self` 或者 `frames` 取到全局对象，但是在 [Web Workers](https://developer.mozilla.org/zh-CN/docs/Web/API/Worker) 中，只有 `self` 可以。在 Node.js 中，它们都无法获取，必须使用 `global`。

在松散模式下，可以在函数中返回 `this` 来获取全局对象，但是在严格模式和模块环境下，`this` 会返回 `undefined`。 You can also use `Function('return this')()`, but environments that disable [`eval()`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/eval), like [CSP](https://wiki.developer.mozilla.org/en-US/docs/Glossary/CSP) in browsers, prevent use of [`Function`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Function) in this way.

`globalThis` 提供了一个标准的方式来获取**不同环境下的全局 `this` 对象**（也就是全局对象自身）。不像 `window` 或者 `self` 这些属性，它确保可以在有无窗口的各种环境下正常工作。所以，你可以安心的使用 `globalThis`，不必担心它的运行环境。为便于记忆，你只需要记住，全局作用域中的 `this` 就是 `globalThis`。

### [HTML 与 WindowProxy](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/globalThis#html_与_windowproxy)

在很多引擎中， `globalThis` 被认为是真实的全局对象的引用，但是在浏览器中，由于 iframe 以及跨窗口安全性的考虑，它实际引用的是真实全局对象（不可以被直接访问）的 [`Proxy`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Proxy) 代理。在通常的应用中，很少会涉及到代理与对象本身的区别，但是也需要加以注意。

![img](/Users/zhitong.ye/Desktop/开发技术笔记/web_frontend_keynotes/全局对象 globalThis.assets/visualization.svg)

## 兼容性

ios 12.2+

![image-20210628172035831](/Users/zhitong.ye/Desktop/开发技术笔记/web_frontend_keynotes/全局对象 globalThis.assets/image-20210628172035831.png)

如果无法兼容，则需使用polyfill来获得全局对象

> 【译】一种震惊的`globalThis` JavaScript polyfill 通用实现方式 - 知乎 https://zhuanlan.zhihu.com/p/336227349

