# 联调代理：Whistle



联调过程中，直接从本地dev-server位置(127.0.0.1:3030)向线上api发起请求，会有跨域问题。这时需要通过代理，将线上环境域名映射到本地dev server，例如下面这样：

```
https://apms.test.shopee.io/ http://127.0.0.1:3000
```

## 工具： Whistle（代理middleware）+ switchyOmega （chrome代理管理插件）

详细教程：http://wproxy.org/whistle/

whistle安装过程需要以下步骤(**缺一不可**)：

1. **安装Node**

2. **安装whistle**

   ```
   $ npm install -g whistle
   ```

   

3. **启动whistle**

4. **配置代理**

5. **安装根证书**（否则无法代理HTTPS）

### 常用命令行

```
w2 run： 开发模式启动 whistle，这种启动方式可以看到插件输出的 console 日志，且会自动加载当前目录的所有插件
w2 start： 正常启动 whistle
w2 stop： 停止 whistle（w2 run 的方式无法用此命令停止）
w2 restart： 重启 whistle（w2 run 的方式无法用此命令重启
```

