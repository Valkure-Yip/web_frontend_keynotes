// myCall
Function.prototype.myCall = function (context, ...args) {
	if (this === Function.prototype) {
		return undefined // 用于防止 Function.prototype.myCall() 直接调用
	}
	context = context || window;
	const fn = Symbol();
	context[fn] = this; // 向目标上下文注入这个方法
	const result = context[fn](...args); // 执行方法
	delete context[fn]; // 删除注入的方法
	return result
}

// myApply
Function.prototype.myApply = function (context, args) {
	if (this === Function.prototype) {
		return undefined; 
	}
	context = context || window
	const fn = Symbol();
	context[fn] = this; // 向目标上下文注入这个方法
	const result = context[fn](...args);
	delete context[fn];
	return result
}

// myBind
Function.prototype.myBind = function (context,...args1) {
	if (this === Function.prototype) {
	  throw new TypeError('Error')
	}
	const _this = this
	return function F(...args2) {
	  // 判断是否用于构造函数
	  if (this instanceof F) {
		return new _this(...args1, ...args2) // 如果构造函数是则使用new调用当前函数
	  }
	  return _this.apply(context, args1.concat(args2))
	}
  }

// e.g.
var objShow = {
	data: 'data_show',
	show(...args) {
		console.log(`show: ${this.data}; args: ${args.join()}`)
	}
}

var objPresent = {
	data: 'data_present',
	present(...args) {
		console.log(`show: ${this.data}; args: ${args.join()}`)
	}
}

objShow.show();
objPresent.present();
// js native
objShow.show.apply(objPresent, [1, 2, 3])
objShow.show.call(objPresent, 4, 5, 6)
objShow.show.bind(objPresent)(7, 8, 9)
// my
objShow.show.myApply(objPresent, [1, 2, 3])
objShow.show.myCall(objPresent, 4, 5, 6)
objShow.show.myBind(objPresent)(7, 8, 9)