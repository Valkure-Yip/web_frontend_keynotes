# Shell

https://www.learnshell.org/en/

### echo

```sh
echo 'Goodbye, World!'
```

### variables

```sh
PRICE_PER_APPLE=5
echo "The price of an Apple today is: \$HK $PRICE_PER_APPLE"
```

${} is used to avoid ambiguity

```bash
MyFirstLetters=ABC
echo "The first 10 letters in the alphabet are: ${MyFirstLetters}DEFGHIJ"
```

 "" will preserve any white space values

```bash
greeting='Hello        world!'
echo $greeting" now with spaces: $greeting"
```

Variables can be assigned with the value of a command output. This is referred to as substitution. Substitution can be done by encapsulating the command with `` (known as back-ticks) or with $()

```bash
FILELIST=`ls`
FileWithTimeStamp=/tmp/my-dir/file_$(/bin/date +%Y-%m-%d).txt
```

### 传参

```bash
#!/bin/bash
echo "File name is "$0 # holds the current script
echo $3 # $3 holds banana
Data=$5
echo "A $Data costs just $6."
echo $#
```

Executing the script on terminal as,

**bash my_shopping.sh apple 5 banana 8 "Fruit Basket" 15**

output is

**File name is my_shopping.sh**

**banana**

**A Fruit Basket costs just 15**

**6**

The variable $# holds the number of arguments passed to the script

The variable $@ holds a space delimited string of all arguments passed to the script



### array

An array is initialized by assign space-delimited values enclosed in ()

```bash
my_array=(apple banana "Fruit Basket" orange)
new_array[2]=apricot
```

The total number of elements in the array is referenced by ${#arrayname[@]}

```bash
my_array=(apple banana "Fruit Basket" orange)
echo  ${#my_array[@]}                   # 4
```

```bash
my_array=(apple banana "Fruit Basket" orange)
echo ${my_array[3]}                     # orange - note that curly brackets are needed
# adding another array element
my_array[4]="carrot"                    # value assignment without a $ and curly brackets
echo ${#my_array[@]}                    # 5
echo ${my_array[${#my_array[@]}-1]}     # carrot
echo ${my_array[@]}                     # apple banana Fruit Basket orange carrot
```



### basic operators

Simple arithmetics on variables can be done using the arithmetic expression: $((expression))

```bash
A=3
B=$((100 * $A + 5)) # 305
```

The basic operators are:

**a + b** addition (a plus b)

**a - b** substraction (a minus b)

**a \* b** multiplication (a times b)

**a / b** division (integer) (a divided by b)

**a % b** modulo (the integer remainder of a divided by b)

**a** ****** **b** exponentiation (a to the power of b)



### string

[Basic String Operations - Learn Shell - Free Interactive Shell Tutorial](https://www.learnshell.org/en/Basic_String_Operations)



### decision

**if [ expression ]; then**

code if 'expression' is true

**fi**

```bash
NAME="John"
if [ "$NAME" = "John" ]; then
  echo "True - my name is indeed John"
fi
```

It can be expanded with 'else'

```bash
NAME="Bill"
if [ "$NAME" = "John" ]; then
  echo "True - my name is indeed John"
else
  echo "False"
  echo "You must mistaken me for $NAME"
fi
```

It can be expanded with 'elif' (else-if)

```bash
NAME="George"
if [ "$NAME" = "John" ]; then
  echo "John Lennon"
elif [ "$NAME" = "George" ]; then
  echo "George Harrison"
else
  echo "This leaves us with Paul and Ringo"
fi
```

The expression used by the conditional construct is evaluated to either true or false. The expression can be a single string or variable. A empty string or a string consisting of spaces or an undefined variable name, are evaluated as false. The expression can be a logical combination of comparisons: negation is denoted by !, logical AND (conjunction) is denoted by &&, and logical OR (disjunction) is denoted by ||. Conditional expressions should be surrounded by double brackets [[ ]].

##### Types of numeric comparisons

```bash
comparison    Evaluated to true when
$a -lt $b    $a < $b
$a -gt $b    $a > $b
$a -le $b    $a <= $b
$a -ge $b    $a >= $b
$a -eq $b    $a is equal to $b
$a -ne $b    $a is not equal to $b
```

##### Types of string comparisons

```bash
comparison    Evaluated to true when
"$a" = "$b"     $a is the same as $b
"$a" == "$b"    $a is the same as $b
"$a" != "$b"    $a is different from $b
-z "$a"         $a is empty
```

- note1: whitespace around = is required
- note2: use "" around string variables to avoid shell expansion of special characters as *

##### case structure

```bash
case "$variable" in
    "$condition1" )
        command...
    ;;
    "$condition2" )
        command...
    ;;
esac
```

##### simple case bash structure

```bash
mycase=1
case $mycase in
    1) echo "You selected bash";;
    2) echo "You selected perl";;
    3) echo "You selected phyton";;
    4) echo "You selected c++";;
    5) exit
esac
```

