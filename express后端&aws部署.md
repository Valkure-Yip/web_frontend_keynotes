# Express 服务端 & AWS 部署



[Build and Deploy a Web Application With React and Node.js+Express | by Leandro Ercoli | Geek Culture | Medium](https://medium.com/geekculture/build-and-deploy-a-web-application-with-react-and-node-js-express-bce2c3cfec32)

[➫ How to npm run start at the background ⭐️ | by Ido Montekyo | idomongo | Medium](https://medium.com/idomongodb/how-to-npm-run-start-at-the-background-%EF%B8%8F-64ddda7c1f1)

pm2: node 进程管理 https://pm2.keymetrics.io/docs/usage/quick-start/

```bash
pm2 --name HelloWorld start npm -- start
```



## Express scaffolding

 [Express application generator](https://expressjs.com/en/starter/generator.html)

```console
$ npm install -g express-generator
$ express
```

Display the command options with the `-h` option:

```console
$ express -h

  Usage: express [options] [dir]

  Options:

    -h, --help          output usage information
        --version       output the version number
    -e, --ejs           add ejs engine support
        --hbs           add handlebars engine support
        --pug           add pug engine support
    -H, --hogan         add hogan.js engine support
        --no-view       generate without view engine
    -v, --view <engine> add view <engine> support (ejs|hbs|hjs|jade|pug|twig|vash) (defaults to jade)
    -c, --css <engine>  add stylesheet <engine> support (less|stylus|compass|sass) (defaults to plain css)
        --git           add .gitignore
    -f, --force         force on non-empty directory
```



## MDN express 教程

Source code: [GitHub - mdn/express-locallibrary-tutorial: Local Library website written in NodeJS/Express; example for the MDN server-side development NodeJS module: https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs.](https://github.com/mdn/express-locallibrary-tutorial)



 **[Express Tutorial Part 2: Creating a skeleton website - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/skeleton_website)**

**Nodemon**: server restart on file change

```bash
npm install --save-dev nodemon
```

**[Express Tutorial Part 3: Using a Database (with Mongoose) - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/mongoose)**

**need to read more about mongoose api**

**[Express Tutorial Part 4: Routes and controllers - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes)**

- "Routes" to forward the supported requests (and any information encoded in request URLs) to the appropriate controller functions.
- Controller functions to get the requested data from the models, create an HTML page displaying the data, and return it to the user to view in the browser.
- Views (templates) used by the controllers to render the data.

![img](/Users/zhitong.ye/Desktop/开发技术笔记/web_frontend_keynotes/express后端&aws部署.assets/mvc_express.png)

- `catalog/` — The home/index page.
- `catalog/<objects>/` — The list of all books, bookinstances, genres, or authors (e.g. /`catalog/books/`, /`catalog/genres/`, etc.)
- `catalog/<object>/<id>` — The detail page for a specific book, bookinstance, genre, or author with the given `_id` field value (e.g. `/catalog/book/584493c1f4887f06c0e67d37)`.
- `catalog/<object>/create` — The form to create a new book, bookinstance, genre, or author (e.g. `/catalog/book/create)`.
- `catalog/<object>/<id>/update` — The form to update a specific book, bookinstance, genre, or author with the given `_id` field value (e.g. `/catalog/book/584493c1f4887f06c0e67d37/update)`.
- `catalog/<object>/<id>/delete` — The form to delete a specific book, bookinstance, genre, author with the given `_id` field value (e.g. `/catalog/book/584493c1f4887f06c0e67d37/delete)`.

**[Express Tutorial Part 5: Displaying library data - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data)**

1. [Asynchronous flow control using async](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/flow_control_using_async)
2. [Template primer](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/Template_primer)
3. [The LocalLibrary base template](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/LocalLibrary_base_template)
4. [Home page](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/Home_page)
5. [Book list page](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/Book_list_page)
6. [BookInstance list page](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/BookInstance_list_page)
7. [Date formatting using luxon](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/Date_formatting_using_moment)
8. [Author list page and Genre list page challenge](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/Author_list_page) [bookmark here 2021.12.24]
9. [Genre detail page](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/Genre_detail_page)
10. [Book detail page](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/Book_detail_page)
11. [Author detail page](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/Author_detail_page)
12. [BookInstance detail page and challenge](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Displaying_data/BookInstance_detail_page_and_challenge)



**[Express Tutorial Part 6: Working with forms - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/forms)**

1. [Create Genre form](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/forms/Create_genre_form) — Defining a page to create `Genre` objects.
2. [Create Author form](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/forms/Create_author_form) — Defining a page to create `Author` objects.
3. [Create Book form](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/forms/Create_book_form) — Defining a page/form to create `Book` objects.
4. [Create BookInstance form](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/forms/Create_BookInstance_form) — Defining a page/form to create `BookInstance` objects.
5. [Delete Author form](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/forms/Delete_author_form) — Defining a page to delete `Author` objects.
6. [Update Book form](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/forms/Update_Book_form) — Defining page to update `Book` objects.



**[Express Tutorial Part 7: Deploying to production - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/deployment)**

Best practice to deploy express app: [Performance Best Practices Using Express in Production](https://expressjs.com/en/advanced/best-practice-performance.html#set-node_env-to-production)

**[deploy] need to read more about env config (dotenv, how to manage diff envs, 12-factor apps):**

aws ec2 codeDeploy + github: https://docs.aws.amazon.com/codedeploy/latest/userguide/tutorials-github.html

**On this one (2021.12.30)** Booktag: https://docs.aws.amazon.com/codedeploy/latest/userguide/tutorials-github-deploy-application.html

> **Note**
>
> If you use one of your revisions instead of our sample revision, your revision must:
>
> - Follow the guidelines in [Plan a revision for CodeDeploy](https://docs.aws.amazon.com/codedeploy/latest/userguide/application-revisions-plan.html) and [Add an application specification file to a revision for CodeDeploy](https://docs.aws.amazon.com/codedeploy/latest/userguide/application-revisions-appspec-file.html).
> - Work with the corresponding instance type.
> - Be accessible from your GitHub dashboard.
>
> If your revision meets these requirements, skip ahead to [Step 5: Create an application and deployment group](https://docs.aws.amazon.com/codedeploy/latest/userguide/tutorials-github-create-application.html).
>
> If you're deploying to an Ubuntu Server instance, you'll need to upload to your GitHub repository a revision compatible with an Ubuntu Server instance and CodeDeploy. For more information, see [Plan a revision for CodeDeploy](https://docs.aws.amazon.com/codedeploy/latest/userguide/application-revisions-plan.html) and [Add an application specification file to a revision for CodeDeploy](https://docs.aws.amazon.com/codedeploy/latest/userguide/application-revisions-appspec-file.html).

[todo] CI: aws elastic beanstalk + gitlab [Deploying Node.js applications to Elastic Beanstalk - AWS Elastic Beanstalk](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_nodejs.html)

deploy to EC2: [How to deploy Node.js app on AWS with GitLab | by Abhinav Dhasmana | Medium](https://adhasmana.medium.com/how-to-deploy-node-js-app-on-aws-with-gitlab-24fabde1088d)

[todo] aws amplify + react

[Node.js Everywhere with Environment Variables! | by John Papa | Node.js Collection | Medium](https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786)

[dotenv - npm](https://www.npmjs.com/package/dotenv): manage env var in .env files (DO NOT commit .env to git)

[The Twelve-Factor App](https://12factor.net/)：put deploy-specific configs to **environment variables**, e.g. NODE_ENV, API_KEY

**[maintenance] need to read more about systemd & pm2 & morgan logging**

### morgan logging:

[Node.js Morgan Guide - DEV Community](https://dev.to/paras594/node-js-morgan-guide-431o) 用morgan和rotating-file-stream的logging机制
