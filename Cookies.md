# Cookies

开发时遇到如下一个需求：需要在APMS portal中嵌入另一个平台页面（Grafana），并使用一个公共账号登录，使得所有有权限访问portal的用户也都能浏览grafana数据，而无需另申请grafana权限。本来探讨是否可能在portal中使用iframe引用，并注入公共账户cookie来登录。因不能注入或修改跨域cookie，最终放弃，改为BE做proxy。



以下内容来自MDN，复习:

[TOC]



### [The `Set-Cookie` and `Cookie` headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies#the_set-cookie_and_cookie_headers)

In HTTP response:

```
Set-Cookie: <cookie-name>=<cookie-value>
```

e.g.

```
HTTP/2.0 200 OK
Content-Type: text/html
Set-Cookie: yummy_cookie=choco
Set-Cookie: tasty_cookie=strawberry

[page content]
```

之后再请求本域名时带上`Cookie` header：

```
GET /sample_page.html HTTP/2.0
Host: www.example.org
Cookie: yummy_cookie=choco; tasty_cookie=strawberry
```



### [Define the lifetime of a cookie](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies#define_the_lifetime_of_a_cookie)

The lifetime of a cookie can be defined in two ways:

- *Session* cookies are deleted when the current session ends. The browser defines when the "current session" ends, and some browsers use *session restoring* when restarting. This can cause session cookies to last indefinitely.
- *Permanent* cookies are deleted at a date specified by the `Expires` attribute, or after a period of time specified by the `Max-Age` attribute.



### [Restrict access to cookies](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies#restrict_access_to_cookies): `Secure`,`HttpOnly`

A cookie with the **`Secure`** attribute is only sent to the server with an encrypted request over the **HTTPS** protocol. It's never sent with unsecured HTTP (except on localhost), which means attackers [man-in-the-middle](https://developer.mozilla.org/en-US/docs/Glossary/MitM) can't access it easily. Insecure sites (with `http:` in the URL) can't set cookies with the `Secure` attribute. However, don't assume that `Secure` prevents all access to sensitive information in cookies. For example, someone with access to the client's hard disk (or JavaScript if the `HttpOnly` attribute isn't set) can read and modify the information.

A cookie with the **`HttpOnly`** attribute is inaccessible to the **JavaScript** [`Document.cookie`](https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie) API; it's only sent to the server. For example, cookies that persist in server-side sessions don't need to be available to JavaScript and should have the `HttpOnly` attribute. This precaution helps mitigate cross-site scripting ([XSS](https://developer.mozilla.org/en-US/docs/Web/Security/Types_of_attacks#cross-site_scripting_(xss))) attacks.



### [Define where cookies are sent](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies#define_where_cookies_are_sent)

The `Domain` and `Path` attributes define the *scope* of a cookie: what URLs the cookies should be sent to.

#### `Domain` attribute

The `Domain` attribute specifies which hosts can receive a cookie. If unspecified, the attribute defaults to the same [host](https://developer.mozilla.org/en-US/docs/Glossary/Host) that set the cookie, *excluding subdomains*. **If `Domain` *is* specified, then subdomains are always included**. Therefore, specifying `Domain` is less restrictive than omitting it. However, it can be helpful when subdomains need to share information about a user.

For example, if you set `Domain=mozilla.org`, cookies are available on subdomains like `developer.mozilla.org`.

#### `Path` attribute

The `Path` attribute indicates a URL path that must exist in the requested URL in order to send the `Cookie` header. The `%x2F` ("/") character is considered a directory separator, and subdirectories match as well.

For example, if you set `Path=/docs`, these request paths match:

- `/docs`
- `/docs/`
- `/docs/Web/`
- `/docs/Web/HTTP`

But these request paths don't:

- `/`
- `/docsets`
- `/fr/docs`

#### `SameSite` attribute

The [`SameSite`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite) attribute lets servers specify whether/when cookies **are sent with cross-site requests** (where [Site](https://developer.mozilla.org/en-US/docs/Glossary/Site) is defined by the registrable domain). This provides some protection against cross-site request forgery attacks ([CSRF](https://developer.mozilla.org/en-US/docs/Glossary/CSRF)). It takes three possible values: `Strict`, `Lax`, and `None`.

With `Strict`, the cookie is only sent to the site where it originated. `Lax` is similar, except that cookies are sent when the user *navigates* to the cookie's origin site. For example, by following a link from an external site. `None` specifies that cookies are sent on both originating and cross-site requests, but *only in secure contexts* (i.e., if `SameSite=None` then the `Secure` attribute must also be set). If no `SameSite` attribute is set, the cookie is treated as `Lax`.

Here's an example:

```
Set-Cookie: mykey=myvalue; SameSite=Strict
```



#### JavaScript access using Document.cookie

You can create new cookies via JavaScript using the [`Document.cookie`](https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie) property. You can access existing cookies from JavaScript as well if the `HttpOnly` flag isn't set.

```js
document.cookie = "yummy_cookie=choco";
document.cookie = "tasty_cookie=strawberry";
console.log(document.cookie);
// logs "yummy_cookie=choco; tasty_cookie=strawberry"
```

Cookies created via JavaScript can't include the `HttpOnly` flag.

Please note the security issues in the [Security](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies#security) section below. Cookies available to JavaScript can be stolen through XSS.

##### Javascript cookie 限制：

**`document.cookies` 无法读到来自其他域名的cookie，无论是否设置HttpOnly**

**`document.cookies` 无法写入网站部署域名以外域名的cookie**



### [Third-party cookies](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies#third-party_cookies)

A cookie is associated with a domain. If this domain is the same as the domain of the page you're on, the cookie is called a ***first-party cookie***. If the domain is different, it's a ***third-party cookie***. While the server hosting a web page sets first-party cookies, the page may contain images or other components stored on servers in other domains (for example, **ad banners**) that may set third-party cookies. These are mainly used for advertising and tracking across the web. For example, the [types of cookies used by Google](https://policies.google.com/technologies/types).

A third-party server can create a profile of a user's browsing history and habits based on cookies sent to it by the same browser when accessing multiple sites. Firefox, by default, blocks third-party cookies that are known to contain trackers. Third-party cookies (or just tracking cookies) may also be blocked by other browser settings or extensions. Cookie blocking can cause some third-party components (such as social media widgets) not to function as intended.



## 好用的库: npm  js-cookie

https://www.npmjs.com/package/js-cookie



## cookie 法规： GDPR & cookie consent

GDPR: [General Data Protection Regulation (GDPR) Compliance Guidelines](https://gdpr.eu/)

react组件：[react-cookie-consent - npm](https://www.npmjs.com/package/react-cookie-consent)