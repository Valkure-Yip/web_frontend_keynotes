# ES5 寄生组合继承

```js
function Super(foo) {
	this.foo = foo;
}
Super.prototype.printFoo = function () {
	console.log(this.foo);
};
function Sub(bar) {
	this.bar = bar;
	// 1. 调用父类的构造函数
	Super.call(this);
}
// 2. 将子类是原型绑定在父类原型的复制对象上，之后修改子类原型就不会影响父类原型
Sub.prototype = Object.create(Super.prototype);
// 3. 让子类原型的构造函数指向子类构造函数
Sub.prototype.constructor = Sub;
```





ES5如何实现继承（附每种继承方式优缺点）_weixin_45844376的博客-CSDN博客_es5实现继承 https://blog.csdn.net/weixin_45844376/article/details/105677231

JS手写代码之寄生组合继承和ES6继承 | CassielLee'Blog https://cassiellee.github.io/2020/04/10/JS%E6%89%8B%E5%86%99%E4%BB%A3%E7%A0%81%E4%B9%8B%E5%AF%84%E7%94%9F%E7%BB%84%E5%90%88%E7%BB%A7%E6%89%BF%E5%92%8CES6%E7%BB%A7%E6%89%BF/



## ES6 继承： 寄生组合式继承 语法糖

```javascript
class Parent {
  constructor(a){
    this.filed1 = a;
  }
  filed2 = 2;
  func1 = function(){}
}

class Child extends Parent {
    constructor(a,b) {
      super(a);
      this.filed3 = b;
    }

  filed4 = 1;
  func2 = function(){}
}
```

## 独享及共享方法

```js
// ES5
function Parent(a){
    this.field1 = a;
    this.funcUnique = function(){} // 每个实例独有方法
}
Parent.prototype.funcShared = function(){} // 实例共享方法
```



```js
// ES6
class Parent {
  constructor(a){
    this.filed1 = a;
    this.funcUnique = function(){} // 每个实例独有方法
  }
  filed2 = 2;
  funcShared = function(){} // 实例共享方法
}
```

一般都将方法定义在prototype上，这样function不会每创建一个实例就多占一份内存