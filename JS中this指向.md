# JS中this指向

> Gentle Explanation of "this" in JavaScript https://dmitripavlutin.com/gentle-explanation-of-this-in-javascript/
>
> 【译文】JS 中 this 在各个场景下的指向 - SegmentFault 思否 https://segmentfault.com/a/1190000020021761



JS 中函数调用主要有以下几种方式：

- 函数调用: `alert('Hello World!')`
- 方法调用: `console.log('Hello World!')`
- 构造函数: `new RegExp('\\d')`
- 隐式调用: `alert.call(undefined, 'Hello World!')`



- **函数调用**：执行构成函数主体的代码：例如，`parseInt`函数调用是`parseInt('15')`。
- **调用的上下文**：指 `this` 在函数体内的值。 例如，`map.set('key', 'value')`的调用上下文是 `map`。
- **函数的作用域**：是在函数体中可访问的变量、对象和函数的集合。



## 函数调用

**this 在函数调用中是一个全局对象**

全局对象由执行环境决定。在浏览器中，`this`是 `window` 对象。

![clipboard.png](https://segmentfault.com/img/bVbv5xi)

在函数调用中，执行上下文是全局对象。

再来看看下面函数中的上下文又是什么鬼：

```js
function sum(a, b) {
   console.log(this === window); // => true
   this.myNumber = 20; // 将'myNumber'属性添加到全局对象
   return a + b;
}
// sum() is invoked as a function
// sum() 中的 `this` 是一个全局对象（window）
sum(15, 16);     // => 31
window.myNumber; // => 20
```

在调用`sum(15,16)`时，JS 自动将`this`设置为全局对象，在浏览器中该对象是`window`。

### 严格模式下的函数调用 this 

启用后，严格模式会影响执行上下文，`this` 在常规函数调用中值为`undefined`。 与上述情况`2.1`相反，执行上下文不再是全局对象。

![clipboard.png](https://segmentfault.com/img/bVbv5y3)

严格模式函数调用示例：

```js
function multiply(a, b) {
  'use strict'; // 启用严格模式
  console.log(this === undefined); // => true
  return a * b;
}
multiply(2, 5); // => 10
```

当`multiply(2,5)`作为函数调用时，`this`是`undefined`。

### 陷阱:`this` 在内部函数中的时候

函数调用的一个常见陷阱是，认为`this`在内部函数中的情况与外部函数中的情况相同。

正确地说，内部函数的上**下文只依赖于它的调用类型**，而不依赖于外部函数的上下文。

```js
const numbers = {
   numberA: 5,
   numberB: 10,
   sum: function() {
     console.log(this === numbers); // => true
     function calculate() {
       console.log(this === numbers); // => false
       return this.numberA + this.numberB;
     }
     return calculate();
   }
};
numbers.sum(); // => NaN 
```

`calculate`函数是在`sum`中定义的，但是：

`calculate()`是一个函数调用(不是方法调用)，它将`this`作为全局对象`window`(非严格模下)。即使外部函数`sum`将上下文作为`number`对象，它在`calculate`里面没有影响。



## 方法调用

区分函数调用和方法调用：

```js
['Hello', 'World'].join(', '); // 方法调用
({ ten: function() { return 10; } }).ten(); // 方法调用
const obj = {};
obj.myFunction = function() {
  return new Date().toString();
};
obj.myFunction(); // 方法调用
/*****************************************/
const otherFunction = obj.myFunction;
otherFunction();     // 函数调用
parseFloat('16.60'); // 函数调用
isNaN(0);            // 函数调用
```



**当调用对象上的方法时，`this`就变成了对象本身。**

![clipboard.png](https://segmentfault.com/img/bVbv5T5)

### 陷阱:将方法与其对象分离

方法可以从对象中提取到一个单独的变量`const alone = myObj.myMethod`。当方法单独调用时，与原始对象`alone()`分离,  如果方法在没有对象的情况下调用，那么函数调用就会发生，此时的`this`指向全局对象`window`严格模式下是`undefined`。

下面的示例定义了`Animal`构造函数并创建了它的一个实例:`myCat`。然后`setTimout()`在1秒后打印`myCat`对象信息

```js
function Animal(type, legs) {
  this.type = type;
  this.legs = legs;  
  this.logInfo = function() {
    console.log(this === myCat); // => false
    console.log('The ' + this.type + ' has ' + this.legs + ' legs');
  }
}
const myCat = new Animal('Cat', 4);
// The undefined has undefined legs 
setTimeout(myCat.logInfo, 1000); 
```

你可能认为`setTimout`调用`myCat.loginfo()`时，它应该打印关于`myCat`对象的信息。

不幸的是，方法在作为参数传递时与对象是分离，`setTimout(myCat.logInfo)`以下情况是等效的：

```js
setTimout(myCat.logInfo);
// 等价于
const extractedLogInfo = myCat.logInfo;
setTimout(extractedLogInfo);
```

将分离的`logInfo`作为函数调用时，`this`是全局 `window`,所以对象信息没有正确地打印。

## 构造函数调用

**在构造函数调用中 this 指向新创建的对象**

![clipboard.png](https://segmentfault.com/img/bVbv8OY)

### 陷阱: 忘了使用 new

```js
function Vehicle(type, wheelsCount) {
  this.type = type;
  this.wheelsCount = wheelsCount;
  return this;
}
// 忘记使用 new 
const car = Vehicle('Car', 4);
car.type;       // => 'Car'
car.wheelsCount // => 4
car === window  // => true
```

`Vehicle`是一个在上下文对象上设置`type`和`wheelsCount`属性的函数。

当执行`Vehicle('Car'， 4)`时，返回一个对象`Car`，它具有正确的属性:`Car.type` 为 `Car`和`Car.wheelsCount` 为`4`，你可能认为它很适合创建和初始化新对象。

然而，在函数调用中，`this`是`window`对象 ，因此 `Vehicle（'Car'，4）`在 `window` 对象上设置属性。 显然这是错误，它并没有创建新对象。

## 箭头函数

箭头函数不会创建自己的执行上下文，而是从**定义它**的外部函数中获取 `this`。 换句话说，箭头函数在词汇上绑定 `this`。

![clipboard.png](https://segmentfault.com/img/bVbv9lg)

### 陷阱: 用箭头函数定义方法

你可能希望使用箭头函数来声明一个对象上的方法。箭头函数的定义相比于函数表达式短得多：`(param) => {...} instead of function(param) {..}`。

来看看例子，用箭头函数在Period类上定义了`format()`方法：

```
function Period (hours, minutes) {  
  this.hours = hours;
  this.minutes = minutes;
}
Period.prototype.format = () => {
  console.log(this === window); // => true
  return this.hours + ' hours and ' + this.minutes + ' minutes';
};
const walkPeriod = new Period(2, 30);  
walkPeriod.format(); // => 'undefined hours and undefined minutes'
```

由于`format`是一个箭头函数，并且在全局上下文（最顶层的作用域）中定义，因此 `this` 指向`window`对象。

即使`format`作为方法在一个对象上被调用如`walkPeriod.format()`，`window`仍然是这次调用的上下文。之所以会这样是因为箭头函数有静态的上下文，并不会随着调用方式的改变而改变。