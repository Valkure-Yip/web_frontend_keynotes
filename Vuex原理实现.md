# Vuex 原理实现

## 使用方式回顾：

```js
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'

// 步骤一，以Vue插件的形式将Vuex引入到我们的项目中
// use调用install方法
Vue.use(Vuex)

// 步骤二，创建一个Store实例
cosnt store = new Vuex.Store({
  // options对象中state, mutations, actions等
  state: {
    counter: 0,
  },
  mutations: {
    add(state) {
      state.counter++
    }
  },
  actions: {
    add({ commit }) {
      setTimeout(() => {
        commit('add')
      }, 1000);
    }
  },
})

// 步骤三，将Store实例添加到Vue根组件实例的选项对象中
new Vue({
  store,
  render: h => h(App)
})
```



## 挂载$store属性：Vue .use() & install()

**Vue.mixin**全局混入**beforeCreate**钩子函数并在钩子函数中实现类似于如下的代码：

```js
this.$store = store // 这里的this指的是当前组件实例
```

这样每个组件实例在创建时都会调用该钩子函数，从而实现将s**tore对象挂载到$store属性**中。

install方法实现：

```js
let Vue

function install(_Vue) {
  Vue = _Vue // 注入Vue
  Vue.mixin({
    beforeCreate() {
      let options = this.$options
      if (options.store) {
        this.$store = options.store
      } else if (options.parent && options.parent.$store) {
        this.$store = options.parent.$store
      }
    }
  })
}
```

## 实现store.state响应式： 创建一个vue实例，作为data

创建一个Vue的实例并把store.state挂载到Vue选项对象的data属性中，这样当创建实例时store.state就会被转变为响应式数据。

为了避免对store.state进行更改，我们**将store.state定义成了取值函数**，这样就**只能对store.state上属性进行修改而不能直接修改store.state。**

> getter - JavaScript | MDN https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/get

```
get state()
仅能读取store.state, 不能赋值
```



```js
let Vue

class Store {
  constructor(options) {
    this._vm = new Vue({
      data: {
        $$state: options.state
      }
    })
  }
  get state() {
    return this._vm.$data.$$state
  }
}

function install(_Vue) {
  Vue = _Vue
  Vue.mixin({
    beforeCreate() {
      let options = this.$options
      if (options.store) {
        this.$store = options.store
      } else if (options.parent && options.parent.$store) {
        this.$store = options.parent.$store
      }
    }
  })
}

export default {Store, install}
```

## 实现getters

通过Vue的计算属性来实现Vuex中的getters。代码如下：

```js
let Vue

class Store {
  constructor(options) {
    this._vm = new Vue({
      data: {
        $$state: options.state
      },
      computed: this.initGetters(options.getters)
    })
  }
  get state() {
    return this._vm.$data.$$state
  }
  get getters() {
    return this._vm
  }
}

Store.prototype.initGetters = function (getters) {
  let computed = {}
  Object.keys(getters).forEach(key => {
    computed[key] = () => getters[key](this.state)
  })
  return computed
}

function install(_Vue) {
  Vue = _Vue
  Vue.mixin({
    beforeCreate() {
      let options = this.$options
      if (options.store) {
        this.$store = options.store
      } else if (options.parent && options.parent.$store) {
        this.$store = options.parent.$store
      }
    }
  })
}


export default { Store, install};
```

getters的实现方式很简单，我们将从Store构造函数中传入的getters属性中的函数进行了一次包装，然后将包装后的getters对象赋值给Vue选项对象的compted属性，这样我们便以Vue计算属性的形式实现了Vuex。此外为了实现以this.$store.getters.key的形式访问到getters中的数据，我们在Store类中也定义了一个名为getters存取器。

## 实现mutation

Vuex中mutatuion的实现原理还是比较简单的，就是当调用commit函数时，根据commit函数的第一个参数来判断是调用mutatuions中的哪个函数然后传参入参数并调用即可。

```js
let Vue

class Store {
  constructor(options) {
    this._vm = new Vue({
      data: {
        $$state: options.state
      },
      computed: this.initGetters(options.getters)
    })
    this._mutations = options.mutations
  }
  get state() {
    return this._vm.$data.$$state
  }
  get getters() {
    return this._vm
  }
  commit = (type, payload) => {
    this._mutations[type](this.state, payload)
  }
}

Store.prototype.initGetters = function (getters) {
  let computed = {}
  Object.keys(getters).forEach(key => {
    computed[key] = () => getters[key](this.state)
  })
  return computed
}

function install(_Vue) {
  Vue = _Vue
  Vue.mixin({
    beforeCreate() {
      let options = this.$options
      if (options.store) {
        this.$store = options.store
      } else if (options.parent && options.parent.$store) {
        this.$store = options.parent.$store

      }
    }
  })
}


export default { Store, install};
```

## 实现action

Vuex支持Action，Action与Mutation作用相同都是用来变更store.state中的数据。但是Action可以包含异步操作，当异步操作执行完成后再以Mutation的方式完成store.state中的数据变更。我们的代码实现如下：

```js
let Vue

class Store {
  constructor(options) {
    this._vm = new Vue({
      data: {
        $$state: options.state
      },
      computed: this.initGetters(options.getters)
    })
    this._mutations = options.mutations
    this._actions = options.actions
  }
  get state() {
    return this._vm.$data.$$state
  }
  get getters() {
    return this._vm
  }
  commit = (type, payload) => {
    this._mutations[type](this.state, payload)
  }
  dispatch = (type, payload) => {
    let context = {
      commit: this.commit,
      state: this.state,
    }
    this._actions[type](context, payload)
  }
}

Store.prototype.initGetters = function (getters) {
  let computed = {}
  Object.keys(getters).forEach(key => {
    computed[key] = () => getters[key](this.state)
  })
  return computed
}

function install(_Vue) {
  Vue = _Vue
  Vue.mixin({
    beforeCreate() {
      let options = this.$options
      if (options.store) {
        this.$store = options.store
      } else if (options.parent && options.parent.$store) {
        this.$store = options.parent.$store

      }
    }
  })
}


export default { Store, install};
```

Vuex中action的实现本质上就是对dispatch方法的实现，dispatch与commit原理差不多，最本质的区别是dispatch在调用对应的action函数时会将scommit方法作为参数传入到action函数中去，这样便可以让action方法做完异步操作后再调用commit方法实现对store.state中数据的修改。