// 防抖
function debounce(fn, wait) {
	var timeout;
	return function (...args) {
		clearTimeout(timeout);
		timeout = setTimeout(() => { fn.apply(this, args) })
	}
}




// 节流
function throttle(fn, wait) {
	var lastCallTime = 0;
	return function (...args) {
		var now = Date.now();
		if (now - lastCallTime > wait) {
			fn.apply(this, args)
			lastCallTime = now
		}
	}
}



// 柯里化
function currying(fn) {
	return function curried(...args) {
		if (args.length >= fn.length) {
			return fn.apply(this, args)
		} else {
			return function (...otherArgs) {
				return curried.apply(this, args.concat(otherArgs))
			}
		}
	}
}



// apply, call
function myApply(fn, context, args) {
	const fnSymbol = Symbol();
	context[fnSymbol] = fn
	var res = context[fnSymbol](...args)
	delete context[fnSymbol]
	return res
}

function myCall(fn, context, ...args) {
	const fnSymbol = Symbol();
	context[fnSymbol] = fn
	var res = context[fnSymbol](...args)
	delete context[fnSymbol]
	return res
}
// 另一种写法
Function.prototype.myApply = function (context, args) {
	const fnSymbol = Symbol();
	context[fnSymbol] = this
	var res = context[fnSymbol](...args)
	delete context[fnSymbol]
	return res
}



// new
function myNew(fn, ...args) {
	var obj = {}
	obj.__proto__ = fn.prototype;
	var res = fn.apply(obj, args);
	return typeof res === 'object' ? res : obj
}



// bind
Function.prototype.myBind = function (context, ...args) {
	const _this = this;
	return (...otherArgs) => this.apply(context, args.concat(otherArgs))

}



// instanceof
function myInstanceof(target, origin) {
	while (target) {
		if (target.__proto__ === origin.prototype) {
			return true
		}
		target = target.__proto__
	}
	return false
}



// map
Array.prototype.myMap = function (fn) {
	let res = []
	let arr = this;
	for (let i in arr) {
		res.push(fn(arr[i]))
	}
	return res
}

// map (用reduce)
Array.prototype.myMap2 = function (fn) {
	var res = [];
	this.reduce(function (pre, cur, index, arr) {
		return res.push(fn(cur, index, arr));
	}, []);
	return res
}



// reduce
Array.prototype.myReduce = function (fn, initVal) {
	var num = initVal == undefined ? this[0] : initVal;
	var i = initVal == undefined ? 1 : 0
	for (i; i < this.length; i++) {
		num = fn(num, this[i], i, this)
	}
	return num
}



// 数组扁平化
function flatten(arr) {
	var res = [];
	for (let i = 0; i < arr.length; i++) {
		const element = arr[i];
		if (Array.isArray(element)) {
			res = res.concat(flatten(element))
		} else {
			res.push(arr[i]);
		}
	}
	return res;
}



// 数组去重
// es6 set
function unique(arr) {
	return Array.from(new Set(arr))
}
// es5 for for splice
function unique(arr) {
	for (let i = 0; i < arr.length; i++) {
		for (let j = 0; j < arr.length; j++) {
			if (arr[j] == arr[i]) {
				arr.splice(j, 1)
				j--;
			}
		}
	}
	return arr
}

// indexof push
function unique(arr) {
	var res = [];
	for (let i = 0; i < arr.length; i++) {
		if (res.indexOf(arr[i]) != -1) {
			res.push(arr[i])
		}
	}
	return res
}

// sort
function unique(arr) {
	var res = [arr[0]];
	arr.sort()
	for (let i = 1; i < arr.length; i++) {
		if (arr[i] !== arr[i - 1]) {
			res.push[arr[i]]
		}
	}
	return res
}

// filter
function unique(arr) {
	return arr.filter(function (item, index, arr) {
		return arr.indexOf(item) === index
	})
}



// 发布订阅
class EventEmitter {
	constructor() {
		this.events = {}
	}
	on(eventName, cb) {
		if (this.events[eventName]) {
			this.events[eventName].push(cb)
		} else {
			this.events[eventName] = [cb]
		}
	}
	emit(eventName) {
		if (this.events[eventName]) {
			for (const cb of events[eventName]) {
				cb()
			}
		}
	}
	remove(eventName, callback) {
		this.events[eventName] = this.events[eventName].filter(cb => cb != callback)
	}
	once(eventName, callback) {
		this.on(eventName, function () {
			callback();
			this.remove(eventName, callback)
		})
	}
}



// promise
// TODO


// 原生ajax
function ajax(method, url) {
	const p = new Promise((resolve, reject) => {
		let xhr = new XMLHttpRequest()
		xhr.open(method, url);
		xhr.onreadystatechange = () => {
			if (xhr.readyState == 4) {
				if (xhr.status == 200 || xhr.status == 304) {
					resolve(JSON.parse(xhr.responseText))
				} else {
					reject('error')
				}
			}
		}
		xhr.send()
	})
	return p
}



// setTimeout 实现 setInterval
function mySetInterval(fn, time) {
	let context = this;
	setTimeout(() => {
		fn.call(context)
		mySetInterval(fn, time)
	}, time);
}


/**
 * 排序
 */

// 快速排序 (return) O(nlogn)
function quickSort(array) {
	if (array.length < 2) {
		return array
	}
	var pivot = array[0]
	var left = [];
	var right = [];
	for (let i = 1; i < array.length; i++) {
		if (array[i] < pivot) {
			left.push(array[i])
		} else {
			right.push(array[i])
		}
	}
	return quickSort(left).concat(pivot, quickSort(right))
}
// 快速排序 （原地改动）
function quickSort(array) {
	var partition = function (start, end) {
		if (start >= end) {
			return
		}
		var pivot = start;
		var index = start + 1;
		for (let i = index; i <= end; i++) {
			if (array[i] < array[pivot]) {
				swap(index, i)
				index++;
			}
		}
		swap(pivot, index - 1);
		partition(start, index - 2);
		partition(index, end);
	}
	var swap = function (a, b) {
		var temp = array[a];
		array[a] = array[b];
		array[b] = temp;
	}
	partition(0, array.length - 1)
}
var arr = [4, 3, 5, 2, 1, 6]
quickSort(arr)
console.log(arr);



// 归并排序 
function mergeSort(arr) {
	if (arr.length < 2) {
		return arr
	}
	var mid = Math.floor(arr.length / 2)
	var left = mergeSort(arr.slice(0, mid));
	var right = mergeSort(arr.slice(mid));
	var res = [];
	// 合并两个有序数组
	while (left.length > 0 && right.length > 0) {
		if (left[0] < right[0]) {
			res.push(left.shift())
		} else {
			res.push(right.shift())
		}
	}
	res = res.concat(left).concat(right)
	return res
}

var arr = [4, 3, 5, 2, 1, 6]
console.log(mergeSort(arr));

// 插入排序
function insertSort(list = []) {
	for (let i = 1, len = list.length; i < len; i++) {
		let j = i - 1;
		let temp = list[i];
		while (j >= 0 && list[j] > temp) {
			list[j + 1] = list[j];
			j = j - 1;
		}
		list[j + 1] = temp;
	}
	return list;
}

// 冒泡排序 bubble sort

function bubbleSort(arr) {
	for (var i = 0; i < arr.length - 1; i++) {
		for (var j = i + 1; j < arr.length; j++) {
			if (arr[i] > arr[j]) {
				var temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
	return arr;
}



// 其他
// 给一个长度为2<=n<=10^5的deque，每次操作取出队首两个元素，将较大的放到队尾，较小的放到队首，求第1<=m<=10^18次操作时取出的元素。

var deque = [4, 2, 3, 5, 1];
function findElement(m) {
	var min = Math.min(...deque);
	var time = 0;
	while (true) {
		if (deque[0] == min) {
			break;
		}
		let a = deque.shift();
		let b = deque.shift();
		if (time == m) {
			return [a, b]
		}
		if (a < b) {
			deque.push(b)
			deque.unshift(a)
		} else {
			deque.push(a)
			deque.unshift(b)
		}
		time++;
	}
	var index = (m - time) % (deque.length - 1)
	return [min, deque[index + 1]]
}

/** selection sort
 * 首先在未排序序列中找到最小（大）元素，存放到排序序列的起始位置，
 * 然后，再从剩余未排序元素中继续寻找最小（大）元素，然后放到已排序序列的末尾。
 * 以此类推，直到所有元素均排序完毕。 
 * @param {number[]} arr 
 * @returns 
 */
 function selectionSort(arr){
	var swap = function(i,j){
		var temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	for (let i = 0; i < arr.length-1; i++) {
		var minPos = i;
		for (let j = i; j < arr.length; j++) {
			if (arr[j] < arr[minPos]){
				minPos = j;
			}
		}
		swap(i, minPos)
	}
	return arr
}