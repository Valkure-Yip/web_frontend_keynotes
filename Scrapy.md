# Scrapy

## install

MacOS: [Installing Python 3 on Mac OS X — The Hitchhiker's Guide to Python](https://docs.python-guide.org/starting/install3/osx/)

1. install python3 with homebrew

   ```
   $ brew install python
   ```

2. set PATH env var

   ```
   # .bashrc OR .zshrc
   export PATH=/usr/local/bin:/usr/local/sbin:$PATH
   ```

3. create a virtualenv & install scrapy with pip

   > package management: [Pipenv &amp; Virtual Environments — The Hitchhiker's Guide to Python](https://docs.python-guide.org/dev/virtualenvs/#virtualenvironments-ref)
   >
   > install Scrapy [Installation guide — Scrapy 2.6.1 documentation](https://docs.scrapy.org/en/latest/intro/install.html#installing-scrapy)