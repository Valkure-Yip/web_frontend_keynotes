var data = {
	key_1: 'aaa',
	key_2: 'bbb'
}

function defineReactive(obj, key, val) {
	var value = val // 闭包
	Object.defineProperty(obj, key, {
		enumerable: true,
		configurable: true,
		get: function reactiveGetter() {
			console.log("get value: " + value)
			return value
		},
		set: function reactiveSetter(newVal) {
			console.log(`set value: ${value} -> ${newVal}`)
			value = newVal
		}
	})
}

for (const key in data) {
	if (Object.hasOwnProperty.call(data, key)) {
		defineReactive(data, key, data[key])
	}
}
data.key_1
data.key_2
data.key_1 = '1';
data.key_1
data.key_2
data.key_2 = '2';
data.key_1
data.key_2
