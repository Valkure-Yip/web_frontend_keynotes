# EventLoop 执行顺序


> 有关eventloop底层原理，Node v11改动 以及 浏览器与Node eventloop区别 最好的解释：
> [Recommended by NodeJS official doc] 系列文章：
> **Post Series By [Deepal Jayasekara](https://dpjayasekara.medium.com/?source=post_page-----1cb67a182810--------------------------------):**
>
> - [Event Loop and the Big Picture](https://blog.insiderattack.net/event-loop-and-the-big-picture-nodejs-event-loop-part-1-1cb67a182810)
> - [Timers, Immediates and Next Ticks](https://blog.insiderattack.net/timers-immediates-and-process-nexttick-nodejs-event-loop-part-2-2c53fd511bb3)
> - [Promises, Next-Ticks, and Immediates](https://blog.insiderattack.net/promises-next-ticks-and-immediates-nodejs-event-loop-part-3-9226cbe7a6aa)
> - [Handling I/O](https://blog.insiderattack.net/handling-io-nodejs-event-loop-part-4-418062f917d1)
> - [Event Loop Best Practices](https://blog.insiderattack.net/event-loop-best-practices-nodejs-event-loop-part-5-e29b2b50bfe2)
> - [New changes to timers and microtasks in Node v11](https://blog.insiderattack.net/new-changes-to-timers-and-microtasks-from-node-v11-0-0-and-above-68d112743eb3)
> - [JavaScript Event Loop vs Node JS Event Loop](https://blog.insiderattack.net/javascript-event-loop-vs-node-js-event-loop-aea2b1b85f5c)



## 有几个Event Queue？：

一个完成的eventloop要按顺序处理如下**事件队列**

4个宏事件（macrotask）队列 （它们是libuv原生实现的）：

- **Expired timers and intervals queue** — consists of callbacks of expired timers added using `setTimeout` or interval functions added using `setInterval`.
- **IO Events Queue** — Completed IO events
- **Immediates Queue** — Callbacks added using `setImmediate` function
- **Close Handlers Queue**— Any `close` event handlers.

2个微事件（microtask）队列（非libuv原生，由NodeJS实现）：

- **Next Ticks Queue** — Callbacks added using `process.nextTick` function
- **Other Microtasks Queue** — Includes other microtasks such as resolved promise callbacks

可以将各种eventLoop简化理解为如下结构：

```
Macrotask:
timer queue: [...]
immdiate queue: [...]

Microtask:
nextTick queue: [...]
promise queue: [...]
```



## EventLoop如何运行？

![img](https://miro.medium.com/max/2880/1*2yXbhvpf1kj5YT-m_fXgEQ.png)

EventLoop按照上图蓝色箭头方向，从Timer event queue开始依次处理宏任务。一共有四个宏任务队列，清空一个队列的过程叫做一个phase。在Node v11.0.0以后， **每处理一个宏任务回调前，就先检查微任务队列**。微任务有两个队列：nextTick 和 other microtask （promise）。**nextTick队列优先级高于promise**。故而先遍历nextTick队列，nextTick队列清空后，再遍历promise队列。所有微任务都执行完毕后，再执行下一个宏任务。

(注：Node v11 以前的版本中，会在每执行一个phase前先检查并清空microtask queue，然后执行整个phase，中间不会再检查microtask，即便过程中又有新的microtask入列。在v11.0.0后，此处表现改为与浏览器保持一致，即如上文所述)

> New Changes to the Timers and Microtasks in Node v11.0.0 ( and above) | by Deepal Jayasekara | Deepal’s Blog https://blog.insiderattack.net/javascript-event-loop-vs-node-js-event-loop-aea2b1b85f5c

如图所示，宏任务循环中，先执行timer队列，再执行immediate队列。于是整个eventloop执行过程如下：

```pseudocode
for timer in timer queue:
	run_microtask();
	执行timer callback
for I/O in I/O queue：
	run_microtask();
	执行timer callback
for immediate in immediate queue：
	run_microtask();
	执行setImmediate callback
for handler in close handler queue：
	run_microtask();
	执行handler

// microtask handling
function run_microtask(){
	for nextTick in nextTick queue:
		run nextTick callback
	for promise in promise queue:
		run promise callback
}
```

示例1：

```js
Promise.resolve().then(() => console.log('promise1 resolved'));
Promise.resolve().then(() => console.log('promise2 resolved'));
Promise.resolve().then(() => {
    console.log('promise3 resolved');
    process.nextTick(() => console.log('next tick inside promise resolve handler'));
});
Promise.resolve().then(() => console.log('promise4 resolved'));
Promise.resolve().then(() => console.log('promise5 resolved'));
setImmediate(() => console.log('set immediate1'));
setImmediate(() => console.log('set immediate2'));

process.nextTick(() => console.log('next tick1'));
process.nextTick(() => console.log('next tick2'));
process.nextTick(() => console.log('next tick3'));

setTimeout(() => console.log('set timeout'), 0);
setImmediate(() => console.log('set immediate3'));
setImmediate(() => console.log('set immediate4'));
```

```
C:\Program Files\nodejs\node.exe .\Untitled-1.js
next tick1
next tick2
next tick3
promise1 resolved
promise2 resolved
promise3 resolved
promise4 resolved
promise5 resolved
next tick inside promise resolve handler
set timeout
set immediate1
set immediate2
set immediate3
set immediate4

```





示例2：

```js
Promise.resolve().then(() => console.log('promise1 resolved'));
Promise.resolve().then(() => console.log('promise2 resolved'));
setTimeout(() => {
    console.log('set timeout3')
    Promise.resolve().then(() => console.log('inner promise3 resolved'));
}, 0);
setTimeout(() => console.log('set timeout1'), 0);
setTimeout(() => console.log('set timeout2'), 0);
Promise.resolve().then(() => console.log('promise4 resolved'));
Promise.resolve().then(() => {
    console.log('promise5 resolved')
    Promise.resolve().then(() => console.log('inner promise6 resolved'));
});
Promise.resolve().then(() => console.log('promise7 resolved'));
```



```
C:\Program Files\nodejs\node.exe .\Untitled-2.js
promise1 resolved
promise2 resolved
promise4 resolved
promise5 resolved
promise7 resolved
inner promise6 resolved
set timeout3
inner promise3 resolved
set timeout1
set timeout2

```



> 以下示例参考了：
> 面试题之Event Loop终极篇 - SegmentFault 思否 https://segmentfault.com/a/1190000019494012
> 其示例代码有参考性，但其对nextTick和await的解释不够详细，最终执行结果也不对
> 我更改了nextTick代码位置，并重新解释，以此篇为准

示例代码：

```js
console.log('1');
async function async1() {
    console.log('2');
    await async2();
    console.log('3');
}
async function async2() {
    console.log('4');
}

// process.nextTick(function() {
//     console.log('5');
// })

setTimeout(function() {
    console.log('6');
    // process.nextTick(function() {
    //     console.log('7');
    // })
    new Promise(function(resolve) {
        console.log('8');
        resolve();
    }).then(function() {
        console.log('9')
    })
    
    process.nextTick(function() {
        console.log('7');
    })
})

async1();

new Promise(function(resolve) {
    console.log('10');
    resolve();
}).then(function() {
    console.log('11');
});

process.nextTick(function() {
    console.log('5');
})

console.log('12');
```

结果：
```
C:\Program Files\nodejs\node.exe .\Untitled-1.js
1
2
4
10
12
5
3
11
6
8
7
9

```



第一轮事件循环流程：

- 整体`script`作为第一个宏任务进入主线程，遇到`console.log`，输出1
- 遇到`async1、async2`函数声明，声明暂时不用管
- 遇到`setTimeout`，其回调函数被分发到宏任务Event Queue中。我们暂且记为`setTimeout1 (6)` 
- 执行`async1`，遇到`console.log(2)`，输出2
- 遇到`await async2`，执行`console.log(4)`, 输出4；await后面的代码（`console.log(3)`）相当于一个`then`微任务，放入微任务Event Queue, 记为`await1 (3)`. **await会让出线程，接下来就会跳出async1函数继续往下执行**
- 遇到`Promise`，`new Promise`直接执行，输出10。`then`被分发到微任务Event Queue中。我们记为`then1 (11)`
- 遇到`process.nextTick`，其回调函数被分发到nexttick queue, **优先执行**。我们记为`nextTick1 (5)`
- 执行`console.log('12')`，输出12

到此时第一个事件循环结束，此时宏任务和微任务事件队列如下：

```
MacroTask: [setTimeout1 (6)]
nextTick: [nextTick1 (5)]
MicroTask: [await1 (3), then1 (11)]
```

此时输出： 

```
1 2 4 10 12
```



第二轮事件循环：

先执行所有microtask：

- `nextTick1`， 输出5
- `await1`， 输出3
- `then1`,  输出11

此时微任务队列清空，执行宏任务`setTimeout1`：

- `console.log(6)` 输出6
- 遇到`Promise`,  执行`console.log('8')`, 输出8。 再执行`resolve`， 将之后的`then`回调函数放入微任务队列，记为`then2 (9)`
- 遇到`process.nextTick`，其回调函数被分发到nexttick queue。我们记为`nextTick2 (7)`

此时宏任务和微任务事件队列如下：

```
MacroTask: []
nexttick: [nextTick2 (7)]
MicroTask: [then2 (9)]
```

此时输出： 

```
1 2 4 10 12 5 3 11 6 8
```

最后清空微任务队列：

输出 7，9



## References

setImmediate vs process.nextTick in NodeJs https://jinoantony.com/blog/setimmediate-vs-process-nexttick-in-nodejs

The Node.js Event Loop, Timers, and process.nextTick() | Node.js https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/

[Recommended by NodeJS official doc] Event Loop and the Big Picture — NodeJS Event Loop Part 1 | by Deepal Jayasekara | Deepal’s Blog https://blog.insiderattack.net/event-loop-and-the-big-picture-nodejs-event-loop-part-1-1cb67a182810