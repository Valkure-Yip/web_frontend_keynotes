# ES6 let/const 暂时性死区

#### 什么是TDZ

Temporal Dead Zone(TDZ)是ES6(ES2015)中对作用域新的专用语义。TDZ名词并没有明确地写在ES6的标准文件中，一开始是出现在ES Discussion讨论区中，是对于某些遇到在区块作用域绑定早于声明语句时的状况时，所使用的专用术语。

近来在项目中遇到一个问题然后才意识到这个TDZ，然后再次拜读了阮一峰老是的ES6详解啊，做出了一些自己的笔记，方便下次review啊，

#### let/const和var的区别

在ES6之前，JS的scope只有两种，全局作用域和函数作用域，但是在ES6种出现了块级作用域，即使用let/const可以定义块级作用域。 那么在ES6的新特性中，最容易看到TDZ作用的就是使用let/const的使用上面。 let/const与var的主要不同有两个地方:

- let/const是使用区块作用域；var是使用函数作用域
- 在let/const声明之前就访问对应的变量与常量，会抛出ReferenceError错误；但在var声明之前就访问对应的变量，则会得到undefined

```
console.log(Vname); // undefined;
console.log(Lname); // ReferenceError
var Vname = 'xiaoxiao';
let Lname = 'xiaoxiao';
复制代码
```

实践证明当我们在未声明之前使用var定义的变量时会得到undefined,但是在使用let未定义的变量时会抛出错误。因为ES6中的let声明的变量是不存在变量提升的作用。

**注意这个例子：**

```js
var x = 10;
if (true) {
    x = 20; // ReferenceError
    let x;
 }
复制代码
```

ES6 明确规定，如果区块中存在let和const命令，这个区块对这些命令声明的变量，从一开始就形成了封闭作用域。凡是在声明之前就使用这些变量，就会报错。 总之，在代码块内，使用let命令声明变量之前，该变量都是不可用的。这在语法上，称为“暂时性死区”（temporal dead zone，简称 TDZ）。

```
if (true) {
  // TDZ开始
  tmp = 'abc'; // ReferenceError
  console.log(tmp); // ReferenceError

  let tmp; // TDZ结束
  console.log(tmp); // undefined

  tmp = 123;
  console.log(tmp); // 123
}
复制代码
```

上面代码中，在let命令声明变量tmp之前，都属于变量tmp的“死区”。

#### typeof的“死区”陷阱

我们都知道使用typeof 可以用来检测给定变量的数据类型，也可以使用它来判断值是否被定义。当返回undefined时表示值未定义； 但是在const/let定义的变量在变量声明之前如果使用了typeof就会报错

```
typeof x; // ReferenceError
let x;
复制代码
```

因为x是使用let声明的，那么在x未声明之前都属于暂时性死区，在使用typeof时就会报错。所以在使用let/const进行声明的变量在使用typeof时不一定安全喔。

```
typeof y; // 'undefined'
复制代码
```

但是可以看出，如果我们只是用了typeof而没有去定义，也是不会报错的，从这粒可以看出只要没有明确规定使用const/let定义之前就是不会出错。

#### 传参的“死区”陷阱

例如下面一段代码，我们在使用

```
function bar(x = y, y = 2) {
  return [x, y];
}

bar(); // 报错
复制代码
```

上面代码中，调用bar函数之所以报错（某些实现可能不报错），是因为参数x默认值等于另一个参数y，而此时y还没有声明，属于”死区“。如果y的默认值是x，就不会报错，因为此时x已经声明了。

```
function bar(x = 2, y = x) {
  return [x, y];
}
bar(); // [2, 2]
复制代码
```

使用var和let声明的另外一种区别。

```
// 不报错
var x = x;

// 报错
let x = x;
// ReferenceError: x is not defined
复制代码
```

受“死区”的影响，使用let声明变量时，只要变量在还没有声明完成前使用，就会报错。上面这行就属于这个情况，在变量x的声明语句还没有执行完成前，就去取x的值，导致报错”x 未定义“。

#### 总结

ES6 规定暂时性死区和let、const语句不出现变量提升，主要是为了减少运行时错误，防止在变量声明前就使用这个变量，从而导致意料之外的行为。这样的错误在 ES5 是很常见的，现在有了这种规定，避免此类错误就很容易了。

总之，暂时性死区的本质就是，只要一进入当前作用域，所要使用的变量就已经存在了，但是不可获取，只有等到声明变量的那一行代码出现，才可以获取和使用该变量。

> 注: TDZ最一开始是为了const所设计的，但后来的对let的设计也是一致的。

> 注: 在ES6标准中，对于const所声明的识别子仍然也经常为variable(变量)，称为constant variable(固定的变量)。以const声明所创建出来的常量，在JS中只是不能再被赋(can't re-assignment)，并不是不可被改变(immutable)的，这两种概念仍然有很大的差异。



作者：yolanda_筱筱
链接：https://juejin.cn/post/6844903753015885831
来源：掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。