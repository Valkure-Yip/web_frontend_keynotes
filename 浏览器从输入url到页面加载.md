

# 浏览器从输入url到页面加载

https://segmentfault.com/a/1190000006879700

## 1. DNS解析

将url解析为ip地址

![DNS解析过程](https://segmentfault.com/img/bVDM45?w=1928&h=1248)

上述图片是查找www.google.com的IP地址过程。首先在本地域名服务器中查询IP地址，如果没有找到的情况下，本地域名服务器会向根域名服务器发送一个请求，如果根域名服务器也不存在该域名时，本地域名会向com顶级域名服务器发送一个请求，依次类推下去。直到最后本地域名服务器得到google的IP地址并把它缓存到本地，供下次查询使用

##### DNS缓存

DNS存在着多级缓存，从离浏览器的距离排序的话，有以下几种: 浏览器缓存，系统缓存，路由器缓存，IPS服务器缓存，根域名服务器缓存，顶级域名服务器缓存，主域名服务器缓存。

##### DNS负载均衡

DNS可以返回一个合适的机器的IP给用户，例如可以根据每台机器的负载量，该机器离用户地理位置的距离等等，这种过程就是**DNS负载均衡**，又叫做**DNS重定向**。大家耳熟能详的**CDN(Content Delivery Network)**就是利用DNS的重定向技术，DNS服务器会返回一个跟用户最接近的点的IP地址给用户，CDN节点的服务器负责响应用户的请求，提供所需的内容。

## HTTP/HTTPS

### 请求行

格式如下:
`Method Request-URL HTTP-Version CRLF`

```text
eg: GET index.html HTTP/1.1
```

常用的方法有: GET, POST, PUT, DELETE, OPTIONS, HEAD。

TODO：

- GET和POST有什么区别？

### 请求报头

请求报头允许客户端向服务器传递请求的附加信息和客户端自身的信息。
PS: 客户端不一定特指浏览器，有时候也可使用Linux下的CURL命令以及HTTP客户端测试工具等。
常见的请求报头有: Accept, Accept-Charset, Accept-Encoding, Accept-Language, Content-Type, Authorization, Cookie, User-Agent等。

### 请求正文

POST, PUT等方法时，通常需要客户端向服务器传递数据。这些数据就储存在请求正文中。在请求包头中有一些与请求正文相关的信息，例如: 现在的Web应用通常采用Rest架构，请求的数据格式一般为json。这时就需要设置Content-Type: application/json。

## 浏览器渲染

![img](https://pic2.zhimg.com/80/v2-120f9c065567bbe1ef732fdcdceb3059_720w.jpg)

## JS 事件机制

![preview](https://segmentfault.com/img/bVC1uE?w=734&h=689/view)