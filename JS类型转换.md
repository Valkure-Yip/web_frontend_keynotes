# 类型转换

## 显示类型转换

显式类型转换方法有 `Number`、`String`、`Boolean`、`parseInt`、`parseFloat`、`toString`

一道面试题：

> 问：为什么 [1, 2, 3].map(parseInt) 返回 [1,NaN,NaN]? 答：parseInt函数的第二个参数表示要解析的数字的基数。该值介于 2 ~ 36 之间。
> 如果省略该参数或其值为 0，则数字将以 10 为基础来解析。如果它以 “0x” 或 “0X” 开头，将以 16 为基数。

### toNumber

其他类型转换到 `number` 类型的规则见下方表格：

![img](https://pic3.zhimg.com/80/v2-dd5419596ae5aca3a4d3dbe3157f2a4e_720w.jpg)

`String` 转换为 `Number` 类型的规则：

1. 如果字符串中只包含数字，那么就转换为对应的数字。
2. 如果字符串中只包含十六进制格式，那么就转换为对应的十进制数字。
3. 如果字符串为空，那么转换为0。
4. 如果字符串包含上述之外的字符，那么转换为 NaN。



### ToBoolean

![img](https://pic1.zhimg.com/80/v2-e76864fa4a00fd419ad68fbb1a51ba30_720w.jpg)

`undefined, null, -0, +0, NaN, ''` => false



### ToString

![img](https://pic2.zhimg.com/80/v2-b60cc74cae46c0ad3b5830de230a45e9_720w.jpg)



## 隐式类型转换

`toPrimitive(input, preferredType?)`

`preferredType`: 期望转换的类型，String 或 Number

若省略`preferredType`

如果`preferredType`为 number：

1、如果`input`为原始值，直接返回这个值

2、如果`input`为对象，调用 `input.valueOf()`, 如果结果为原始值，返回

3、否则，调用`input.toString()`, 如果结果为原始值，返回

4、否则，抛出错误

如果`preferredType`为 string：交换上述2，3步

```js
[].valueOf // []
[].toString // ''
({}).valueOf // {}
({}).toString // "[object Object]"
```



### `+`

两边无`string` ： 转为Number

有`String`: 转为字符串concat

### `- * / %`

转为Number计算

### `|| &&`

Boolean()转为布尔值，但返回的是原值

```js
5 && 3 // 3
0 || 2 // 2
```





> 帮你彻底弄懂 JavaScript 类型转换 - 知乎 https://zhuanlan.zhihu.com/p/85731460
>
> 深入理解JS的类型、值、类型转换 · Issue #34 · amandakelake/blog https://github.com/amandakelake/blog/issues/34

