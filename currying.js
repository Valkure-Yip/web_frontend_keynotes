function currying(fn) {
	// 收集参数
	return function curried(...args) {
		// 若参数量大于等于原函数
		if (args.length >= fn.length) {
			return fn.apply(this, args)
		} else {
			// 若参数量不够，继续收集参数
			return function (...otherArgs) {
				return curried.apply(this, args.concat(otherArgs))
			}
		}
	}
}

function add(a, b, c) {
	return a + b + c
}
var curriedAdd = currying(add)

console.log(curriedAdd(1, 2, 3)); // 6，仍然可以被正常调用
console.log(curriedAdd(1)(2, 3)); // 6，对第一个参数的柯里化
console.log(curriedAdd(1)(2)(3)); // 6，全柯里化
console.log(curriedAdd(1, 2)(3)) // 6