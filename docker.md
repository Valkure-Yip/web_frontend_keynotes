# Docker

官方文档：[Orientation and setup | Docker Documentation](https://docs.docker.com/get-started/)

网络教程：[前言 - Docker —— 从入门到实践](https://yeasy.gitbook.io/docker_practice/)

[todo] deploy docker to AWS ec2: https://docs.docker.com/cloud/ecs-integration/

[TOC]



## What is a container?[¶](http://localhost/tutorial/#what-is-a-container)

Now that you've run a container, what *is* a container? Simply put, a container is simply another process on your machine that has been isolated from all other processes on the host machine. That isolation leverages [kernel namespaces and cgroups](https://medium.com/@saschagrunert/demystifying-containers-part-i-kernel-space-2c53d6979504), features that have been in Linux for a long time. Docker has worked to make these capabilities approachable and easy to use.

## What is a container image?[¶](http://localhost/tutorial/#what-is-a-container-image)

When running a container, it uses an isolated filesystem. This custom filesystem is provided by a **container image**. Since the image contains the container's filesystem, it must contain everything needed to run an application - all dependencies, configuration, scripts, binaries, etc. The image also contains other configuration for the container, such as environment variables, a default command to run, and other metadata.



[Our Application - Getting Started](http://localhost/tutorial/our-application/)

## Building the App's Container Image[¶](http://localhost/tutorial/our-application/#building-the-apps-container-image)

In order to build the application, we need to use a `Dockerfile`. 

1. Create a file named `Dockerfile` in the same folder as the file `package.json` with the following contents.

   ```dockerfile
   FROM node:12-alpine
   # Adding build tools to make yarn install work on Apple silicon / arm64 machines
   RUN apk add --no-cache python2 g++ make
   WORKDIR /app
   COPY . .
   RUN yarn install --production
   CMD ["node", "src/index.js"]
   ```

   

2. build the container image using the `docker build` command.

   ```
   docker build -t getting-started .
   ```

   This command used the Dockerfile to build a new container image. You might have noticed that a lot of "layers" were downloaded. This is because we instructed the builder that we wanted to start from the `node:12-alpine` image. But, since we didn't have that on our machine, that image needed to be downloaded.

   After the image was downloaded, we copied in our application and used `yarn` to install our application's dependencies. The `CMD` directive specifies the default command to run when starting a container from this image.

   Finally, the `-t` flag tags our image. Think of this simply as a human-readable name for the final image. Since we named the image `getting-started`, we can refer to that image when we run a container.

   The `.` at the end of the `docker build` command tells that Docker should look for the `Dockerfile` in the current directory.



## Remote Repo

### Create a repo

To push an image, we first need to create a repository on Docker Hub.

1. [Sign up](https://www.docker.com/pricing?utm_source=docker&utm_medium=webreferral&utm_campaign=docs_driven_upgrade) or Sign in to [Docker Hub](https://hub.docker.com/).

2. Click the **Create Repository** button.

3. For the repo name, use `getting-started`. Make sure the Visibility is `Public`.

   > **Private repositories**
   >
   > Did you know that Docker offers private repositories which allows you to restrict content to specific users or teams? Check out the details on the [Docker pricing](https://www.docker.com/pricing?utm_source=docker&utm_medium=webreferral&utm_campaign=docs_driven_upgrade) page.

4. Click the **Create** button!

If you look on the right-side of the page, you’ll see a section named **Docker commands**. This gives an example command that you will need to run to push to this repo.

![Docker command with push example](/Users/zhitong.ye/Desktop/开发技术笔记/web_frontend_keynotes/docker.assets/push-command.png)

### Push the image

[Share the application | Docker Documentation](https://docs.docker.com/get-started/04_sharing_app/)

1. Login to the Docker Hub using the command `docker login -u YOUR-USER-NAME`.

2. Use the `docker tag` command to give the `getting-started` image a new name. Be sure to swap out `YOUR-USER-NAME` with your Docker ID.

   ```bash
    $ docker tag getting-started YOUR-USER-NAME/getting-started
   ```

3. Now try your push command again. If you’re copying the value from Docker Hub, you can drop the `tagname` portion, as we didn’t add a tag to the image name. If you don’t specify a tag, Docker will use a tag called `latest`.

   ```bash
    $ docker push YOUR-USER-NAME/getting-started
   ```



## Starting an App Container[¶](http://localhost/tutorial/our-application/#starting-an-app-container)

Now that we have an image, let's run the application! To do so, we will use the `docker run` command (remember that from earlier?).

1. Start your container using the `docker run` command and specify the name of the image we just created:

   ```
   docker run -dp 3000:3000 getting-started
   ```

   Remember the `-d` and `-p` flags? We're running the new container in "detached" mode (in the background) and creating a mapping between the host's port 3000 to the container's port 3000. Without the port mapping, we wouldn't be able to access the application.

2. After a few seconds, open your web browser to [http://localhost:3000](http://localhost:3000/). You should see our app!



### to start a container with multiple port mappings & auto restart

```bash
docker run -d -p 80:80 -p 3001:3001 --restart always valkure/ami-de-journalistes
```





## Persist DB: volume

By default, the todo app stores its data in a [SQLite Database](https://www.sqlite.org/index.html) at `/etc/todos/todo.db` in the container’s filesystem.

1. Create a volume by using the `docker volume create` command.

   ```bash
    $ docker volume create todo-db
   ```

2. Stop and remove the todo app container once again in the Dashboard (or with `docker rm -f <id>`), as it is still running without using the persistent volume.

3. Start the todo app container, but add the **`-v` flag** to specify a volume mount. We will use the named volume and mount it to `/etc/todos`, which will capture all files created at the path.

   ```bash
    $ docker run -dp 3000:3000 -v todo-db:/etc/todos getting-started
   ```



A lot of people frequently ask “Where is Docker *actually* storing my data when I use a named volume?” If you want to know, you can use the `docker volume inspect` command.

```bash
$ docker volume inspect todo-db
[
    {
        "CreatedAt": "2019-09-26T02:18:36Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/todo-db/_data",
        "Name": "todo-db",
        "Options": {},
        "Scope": "local"
    }
]
```

The `Mountpoint` is the actual location on the disk where the data is stored. Note that on most machines, you will need to have root access to access this directory from the host. But, that’s where it is!



## Bind mounts

[Use bind mounts | Docker Documentation](https://docs.docker.com/get-started/06_bind_mounts/)

With **bind mounts**, we control the exact mountpoint on the host. We can use this to persist data, but it’s often used **to provide additional data into containers**. When working on an application, we can use a bind mount to mount our source code into the container to **let it see code changes, respond, and let us see the changes** right away.

**Named Volumes VS Bind Mounts**

| Named Volumes                                | Bind Mounts               |                               |
| :------------------------------------------- | :------------------------ | ----------------------------- |
| Host Location                                | Docker chooses            | You control                   |
| Mount Example (using `-v`)                   | my-volume:/usr/local/data | /path/to/data:/usr/local/data |
| Populates new volume with container contents | Yes                       | No                            |
| Supports Volume Drivers                      | Yes                       | No                            |

### start a dev-mode container

use `nodemon` to watch for dev changes

```bash
 docker run -dp 3000:3000 \
     -w /app -v "$(pwd):/app" \
     node:12-alpine \
     sh -c "yarn install && yarn run dev"
```

- `-dp 3000:3000` - same as before. Run in detached (background) mode and create a port mapping
- `-w /app` - sets the “working directory” or the current directory that the command will run from
- `-v "$(pwd):/app"` - bind mount the current directory from the host in the container into the `/app` directory
- `node:12-alpine` - the image to use. Note that this is the base image for our app from the Dockerfile
- `sh -c "yarn install && yarn run dev"` - the command. We’re starting a shell using `sh` (alpine doesn’t have `bash`) and running `yarn install` to install *all* dependencies and then running `yarn run dev`. If we look in the `package.json`, we’ll see that the `dev` script is starting `nodemon`.



## multiple container & network

[Multi container apps | Docker Documentation](https://docs.docker.com/get-started/07_multi_container/)

> If two containers are on the same network, they can talk to each other. If they aren’t, they can’t.

#### start a mysql container on a network

1. Create the network.

   ```
    $ docker network create todo-app
   ```

2. Start a MySQL container and attach it to the network. We’re also going to define a few environment variables that the database will use to initialize the database (see the “Environment Variables” section in the [MySQL Docker Hub listing](https://hub.docker.com/_/mysql/)).

   `--network todo-app --network-alias mysql`

   ```bash
    $ docker run -d \
        --network todo-app --network-alias mysql \
        -v todo-mysql-data:/var/lib/mysql \
        -e MYSQL_ROOT_PASSWORD=secret \
        -e MYSQL_DATABASE=todos \
        mysql:5.7
   ```

#### use mysql in todo-app

The todo app supports the setting of a few environment variables to specify MySQL connection settings. They are:

- `MYSQL_HOST` - the hostname for the running MySQL server
- `MYSQL_USER` - the username to use for the connection
- `MYSQL_PASSWORD` - the password to use for the connection
- `MYSQL_DB` - the database to use once connected

start the todo-app with right config

We’ll specify each of the environment variables above, as well as connect the container to our app network.

```bash
 $ docker run -dp 3000:3000 \
   -w /app -v "$(pwd):/app" \
   --network todo-app \
   -e MYSQL_HOST=mysql \
   -e MYSQL_USER=root \
   -e MYSQL_PASSWORD=secret \
   -e MYSQL_DB=todos \
   node:12-alpine \
   sh -c "yarn install && yarn run dev"
```



## docker compose (automate the above process!)

[Docker Compose](https://docs.docker.com/compose/) is a tool that was developed to help define and share multi-container applications. With Compose, we can create a YAML file to define the services and with a single command, can spin everything up or tear it all down.

#### app service:

```bash
 docker run -dp 3000:3000 \
  -w /app -v "$(pwd):/app" \
  --network todo-app \
  -e MYSQL_HOST=mysql \
  -e MYSQL_USER=root \
  -e MYSQL_PASSWORD=secret \
  -e MYSQL_DB=todos \
  node:12-alpine \
  sh -c "yarn install && yarn run dev"
```

Docker-compose.yaml:

```yaml
 version: "3.7"

 services:
   app:
     image: node:12-alpine
     command: sh -c "yarn install && yarn run dev"
     ports:
       - 3000:3000
     working_dir: /app
     volumes:
       - ./:/app
     environment:
       MYSQL_HOST: mysql
       MYSQL_USER: root
       MYSQL_PASSWORD: secret
       MYSQL_DB: todos
```

#### mysql service:

```bash
 docker run -d \
  --network todo-app --network-alias mysql \
  -v todo-mysql-data:/var/lib/mysql \
  -e MYSQL_ROOT_PASSWORD=secret \
  -e MYSQL_DATABASE=todos \
  mysql:5.7
```

Docker-container:

```yaml
 version: "3.7"

 services:
   app:
     # The app service definition
   mysql:
     image: mysql:5.7
     volumes:
       - todo-mysql-data:/var/lib/mysql
     environment:
       MYSQL_ROOT_PASSWORD: secret
       MYSQL_DATABASE: todos

 volumes:
   todo-mysql-data:
```



The whole yml file:

```yaml
version: '3.7'

services:
    app:
        image: node:12-alpine
        command: sh -c "yarn install && yarn run dev"
        ports:
            - 3000:3000
        working_dir: /app
        volumes:
            - ./:/app
        environment:
            MYSQL_HOST: mysql
            MYSQL_USER: root
            MYSQL_PASSWORD: secret
            MYSQL_DB: todos

    mysql:
        image: mysql:5.7
        volumes:
            - todo-mysql-data:/var/lib/mysql
        environment:
            MYSQL_ROOT_PASSWORD: secret
            MYSQL_DATABASE: todos

volumes:
    todo-mysql-data:

```





#### Run application stack

Start up the application stack using the `docker-compose up` command. We’ll add the `-d` flag to run everything in the background.

```
 $ docker-compose up -d
```

When we run this, we should see output like this:

```
 Creating network "app_default" with the default driver
 Creating volume "app_todo-mysql-data" with default driver
 Creating app_app_1   ... done
 Creating app_mysql_1 ... done
```